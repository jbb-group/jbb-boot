# New domain feature checklist

### General
* [ ] A new domain package `org.jbb.boot.<newDomainName>` provided
* [x] Technical code (in `org.jbb.swanframework`) don't have any dependencies to `org.jbb.boot.<newDomainName>` (checked by architecture tests)
* [ ] Test steps of the pipeline are green

### Kotlin API
* [x] All classes under `org.jbb.boot.<newDomainName>.api` are public (checked by architecture
  tests)
* [ ] `api` package contains kotlin files - each file is a single Use case.
  - Each Use case should be a separate thing as much as possible
  - Use case file contains operation definitions (interfaces), possible domain errors (exceptions),
    model classes and event definitions (if applicable)
* [ ] Interface for business actions are named with `*Facade` convention
  - `get*`/`find*` methods only read data without side effects,
  - `create*`/`update*`/`delete*` methods change state of the system
* [ ] Interface's methods cannot throw any business exceptions. Instead, they should
  return `arrow.core.Either<BusinessException, SuccessfullResult>` object
* [ ] Model classes which are built by client should have all necessary `javax.validation`
  annotations
* [x] All business exceptions are runtime and they don't have `*Exception` suffix (checked by
  architecture tests)

### Domain implementation

* [ ] All classes under `org.jbb.boot.<newDomainName>.domain` have internal access modifiers unless
  you have a good reason to be public
* [ ] Think if new domain should have some installation step (implement new `InstallationAction<T>`
  if necessary)
* [ ] Think if new domain should react to any existing `DomainEvent`(s)
* [ ] Think if new domain should implement some `DomainHealthCheck`(s)
* [ ] If new domain introduces a new database entity then all JPA annotations are in place. For columns naming snake_convention is being used

### Web (REST API) implementation
* [ ] Each `*Resource` uses `*Dto` classes as request/response body, not API classes
* [ ] `*Resource` controllers don't have any business logic
* [ ] Transformation logic from DTO to API classes (and in the opposite direction) are extracted to
  separate translator components which are being called by controllers
* [ ] Each business exception is being registered in some `*ErrorInfo` class
* [ ] Each business exception has method in domain  `RestExceptionHandler` which transforms it into
  proper error info code
* [x] Every new endpoint has `@PreAuthorize` annotation with the proper access level
  information (`SecurityConstants.(PERMIT_ALL|IS_AUTHENTICATED|IS_AN_ADMINISTRATOR`) (checked by
  architecture tests)
* [ ] Each system change operation (POST / PUT / PATCH / DELETE) supports `dryRun` mode unless it's
  really hard to achieve this (annotation `@DryRunnable`)
* [ ] For every new endpoint think if it can be available before installation (annotation `@AvailableBeforeInstallation`)
* [ ] For every new endpoint think if it can be available for member with expired password (annotation `@ExpiredPasswordIgnored`)

### Quality assurance
* [ ] At least one happy scenario of REST API in component testing (
  extends `AbstractBaseResourceIT`) has been provided
* [ ] At least one happy scenario of REST API in E2E testing has been provided (if it makes sense in
  black box tests)
* [ ] The Quality gate on [Sonarqube](https://sonarcloud.io/dashboard?id=jbb-boot) for a branch has
  passed

### Release process
* [ ] Think about running `mark minor release` pipeline step when it's quite big feature

Closes #GITLAB_ISSUE_NUMBER