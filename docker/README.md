# 🐳 Docker support

Before you start, make sure you have **Docker** and **Docker Compose** installed in your system. You
can check it by execute following commands:

```
docker -v
```

```
docker-compose version
```

## Introduction

Three configurations have been prepared:

* **Plain compose**
    - Fast start-up of a single instance of the application
    - No installation is being performed. User is able to choose installation details by itself
    - Great for smoke testing if app is able to start in a fresh environment
* **Development compose**
    - Single instance of the application with performing the auto-installation
    - Auto-installation uses embedded versions of external systems (e.g. H2)
    - Great for prototyping, PoC and testing. Not recommended in production
* **Production compose**
    - A configuration that resembles a production deployment. It consists of multiple containers
      with the app, separate load-balancer.
    - It performs auto-installation as well with choosing production versions of external systems (e.g. PostgreSQL)
    - Great for checking if the app works well in "real" environment

## Comparison

|                           | Plain compose                                | Development compose             | Production compose                     |
|---------------------------|----------------------------------------------|---------------------------------|----------------------------------------|
| Start command             | `./gradlew composeUp`                        | `./gradlew devComposeUp`        | `./gradlew prdComposeUp`               |
| Stop command              | `./gradlew composeDown`                      | `./gradlew devComposeDown`      | `./gradlew prdComposeDown`             |
| Java version              | 11                                           | 11                              | 11                                     |
| Automatic installation    | No                                           | Yes                             | Yes                                    |
| Administrator credentials | N/A                                          | `administrator`/`administrator` | `administrator`/`administrator`        |
| Database type             | Up to you, when you perform the installation | H2                              | PostgreSQL                             |
| Number of app instances   | 1                                            | 1                               | 2                                      |
| NGINX before instances    | No                                           | No                              | Yes, exposed on port 80                |
| Application URL for host  | http://localhost:8080                        | http://localhost:8080           | http://localhost (NGINX LB)            |
| Database URL for host     | None                                         | None                            | `postgresql://localhost:5433/default`  |
| Database credentials      | N/A                                          | `dba`/`dba`                     | `dba`/`dba`                            |

### Notes

* Every "Start command" triggers `bootBuildImage` task which builds optimized container image
* [Here](https://reflectoring.io/spring-boot-docker/) you can read more about layer concept and why
  it speeds up image building
* [gradle-docker-compose-plugin](https://github.com/avast/gradle-docker-compose-plugin) is removing
  containers during down phrase by default. It means, your data will be destroyed after each
  restart. To avoid this, change `removeContainers` property value
* Version of the used JRE can be overridden with `-PdockerJvmVersion` (e.g. `8`, `11`, `17`). You
  can check available options
  in [OpenJDK Liberica buildpack releases](https://github.com/paketo-buildpacks/bellsoft-liberica/releases)