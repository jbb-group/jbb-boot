## Branching model
This project follows [GitHub flow](https://guides.github.com/introduction/flow/). Just make your branch name descriptive and open a [Merge Request](https://gitlab.com/jbb-group/jbb-boot/-/merge_requests/new) to the `master` when it's ready.

## IDE Configuration
IntelliJ IDEA is a preferred IDE for development of this project. If you're using different tool please follow manual instructions below.

### Copyright setup
Copyright config is already committed to the repository in the `.idea/copyright` directory. 
Intellij should take a configuration after first opening project automatically. 

Remember to have checkbox "Update copyright" in commit window active!

#### Manual instruction
Go to `Copyright` settings. Add a new Copyright Profile with text:
```
Copyright (C) ${today.year} the original author or authors.

This file is part of jBB Application Project.

 Licensed under the Apache License, Version 2.0 (the "License");
 You may obtain a copy of the License at
       http://www.apache.org/licenses/LICENSE-2.0
```

Set:
* `Regexp to detext copyright in comments` to `Copyright`
* `Allow replacing copyright if old copyright matches` to `jBB`

it will update copyright every year continuously. 

Apply profile to the Java files only.

### Code style setup
Code style config is already committed to the repository in the `.idea/codeStyles` directory. 
Intellij should take a configuration after first opening project automatically. 

Remember to have checkbox "Reformat code" in commit window active!

jBB code style is based
on [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html)
and [Google Kotlin Style Guide](https://developer.android.com/kotlin/style-guide). Only indent size
and tab size have been increased to 4.

#### Manual instruction

1. Download XML
   from [here](https://github.com/google/styleguide/blob/gh-pages/intellij-java-google-style.xml).
   Go to `Code Style` settings and import scheme.
1. For Java and Kotlin increase indent size and tab size to 4
1. For Kotlin choose "Set from..." and click "Kotlin style guide"

### Automatic Restart

Project has integrated
a [Spring Developer Tools](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#using-boot-devtools)
. It means if you run the app in the IDE and change any code your changes will be visible
immediately.

If you want to use that feature remember to set a proper _"Running Application Update Policies"_ in
your _"Run/Debug configuration"_ - [see](https://intellij-support.jetbrains.com/hc/en-us/community/posts/360003378800/comments/360000685039). 