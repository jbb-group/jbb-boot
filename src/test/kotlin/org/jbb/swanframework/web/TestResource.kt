/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web

import io.swagger.v3.oas.annotations.Hidden
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import java.time.Instant
import javax.validation.Valid
import javax.validation.constraints.Min

@Hidden
@RequestMapping(value = ["/api/exception-test"], produces = [MediaType.APPLICATION_JSON_VALUE])
internal class TestResource {
    @GetMapping("/internal-error")
    fun error() {
        throw IllegalStateException("error")
    }

    @PostMapping(value = ["/unsupported-media-type"], consumes = [MediaType.APPLICATION_XML_VALUE])
    fun unsupportedMediaType(@Suppress("UNUSED_PARAMETER") @RequestBody `object`: Any?) {
    }

    @GetMapping(value = ["/not-acceptable-media-type"], produces = [MediaType.APPLICATION_XML_VALUE])
    fun notAcceptableMediaType() {
    }

    @GetMapping("/missing-path-variable/{foo}")
    fun pathVariable(@Suppress("UNUSED_PARAMETER") @PathVariable("bar") foo: String) {
    }

    @GetMapping("/missing-parameter")
    fun missingRequestParameter(@Suppress("UNUSED_PARAMETER") @RequestParam(name = "param") param: Long) {
    }

    @GetMapping("/not-writable")
    @ResponseBody
    fun notWritable(): Foo {
        val foo = Foo()
        val bar = Bar()
        bar.foo = foo
        foo.bar = bar
        return foo
    }

    @PostMapping(value = ["/unrecognized-property"])
    fun unrecognizedProperty(@Suppress("UNUSED_PARAMETER") @RequestBody `object`: Foo?) {
    }

    @PostMapping(value = ["/invalid-format"])
    fun invalidFormat(@Suppress("UNUSED_PARAMETER") @RequestBody dateInfo: DateInfo?) {
    }

    @PostMapping(value = ["/json-parsing-issue"])
    fun jsonParsingError(@Suppress("UNUSED_PARAMETER") @RequestBody dateInfo: DateInfo?) {
    }

    @PostMapping(value = ["/validation-error"])
    fun jsonParsingError(@Suppress("UNUSED_PARAMETER") @RequestBody @Valid data: ValidAge?) {
    }

    data class Foo(var bar: Bar? = null)

    data class Bar(var foo: Foo? = null)

    data class DateInfo(val createdAt: Instant? = null)

    data class DateInfoString(val createdAt: String? = null)

    data class ValidAge(@field:Min(18) val value: Int? = null)
}