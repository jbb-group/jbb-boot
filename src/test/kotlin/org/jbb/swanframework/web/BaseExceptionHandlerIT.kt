/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web

import com.fasterxml.jackson.databind.ObjectMapper
import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.assertj.core.api.Assertions.assertThat
import org.jbb.swanframework.BaseSwanIT
import org.jbb.swanframework.web.TestResource.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import java.io.IOException
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpRequest.BodyPublishers
import java.net.http.HttpResponse.BodyHandlers

@Import(TestEndpointsConfiguration::class)
internal class BaseExceptionHandlerIT : BaseSwanIT() {
    @LocalServerPort
    private val port: Long? = null

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Test
    fun shouldChooseInternalErrorCode_whenRuntimeExceptionThrown() {
        // when
        val response = given()
                .`when`()["/api/exception-test/internal-error"]

        // then
        assertErrorInfo(response, CommonErrorInfo.INTERNAL_ERROR)
    }

    @Test
    fun shouldChooseMethodNotSupportedCode_whenMethodIsNotSupported() {
        // when
        val response = given()
                .`when`().post("/api/exception-test/internal-error")

        // then
        assertErrorInfo(response, CommonErrorInfo.METHOD_NOT_SUPPORTED)
    }

    @Test
    fun shouldChooseUnsupportedMediaTypeCode_whenUnsupportedMediaTypeSupported() {
        // when
        val response = given()
                .`when`().post("/api/exception-test/unsupported-media-type")

        // then
        assertErrorInfo(response, CommonErrorInfo.UNSUPPORTED_MEDIA_TYPE)
    }

    @Test
    fun shouldChooseNotAcceptableMediaTypeCode_whenNotAcceptableMediaTypeProvided() {
        // when
        val response = given()
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .`when`()["/api/exception-test/not-acceptable-media-type"]

        // then
        assertErrorInfo(response, CommonErrorInfo.NOT_ACCEPTABLE_MEDIA_TYPE)
    }

    @Test
    fun shouldChooseMissingPathVariableCode_whenMissingPathVariableNotProvided() {
        // when
        val response = given()
                .`when`()["/api/exception-test/missing-path-variable/3"]

        // then
        assertErrorInfo(response, CommonErrorInfo.MISSING_PATH_VARIABLE)
    }

    @Test
    fun shouldChooseMissingRequestParameterCode_whenRequiredParameterNotProvided() {
        // when
        val response = given()
                .`when`()["/api/exception-test/missing-parameter"]

        // then
        assertErrorInfo(response, CommonErrorInfo.MISSING_REQUEST_PARAMETER)
    }

    @Test
    fun shouldChooseTypeMismatchCode_whenMismatchedTypeProvided() {
        // when
        val response = given()
                .`when`()["/api/exception-test/missing-parameter?param=asd"]

        // then
        assertErrorInfo(response, CommonErrorInfo.TYPE_MISMATCH, ErrorDetail("param",
                "path-variable-type-invalid", "failed to convert path variable to required type"))
    }

    // TODO something is wrong with output stream when circular POJO provided. To be investigated
    @Test
    fun shouldMessageNotWritableCode_whenPojoHasCycle() {
        given().`when`()["/api/exception-test/not-writable"]
    }

    @Test
    fun shouldChooseUnrecognizedPropertyCode_whenUnrecognizedPropertyProvided() {
        // when
        val response = given()
                .body(Bar())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().post("/api/exception-test/unrecognized-property")

        // then
        assertErrorInfo(response, CommonErrorInfo.UNRECOGNIZED_PROPERTY,
                ErrorDetail("property",
                        null, "foo"))
    }

    @Test
    fun shouldChooseInvalidFormatCode_whenInvalidFormatProvided() {
        // when
        val response = given()
                .body(DateInfoString("08.06.2002 15:41:12"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().post("/api/exception-test/invalid-format")

        // then
        assertErrorInfo(response, CommonErrorInfo.INVALID_FORMAT_PROPERTY,
                ErrorDetail("property", null, "createdAt"))
    }

    @Test
    @Throws(IOException::class, InterruptedException::class)
    fun shouldChooseJsonParsingErrorCode_whenInvalidJsonProvided() {
        // when
        val responseBody = HttpClient.newHttpClient()
                .send(HttpRequest.newBuilder().uri(
                        URI.create("http://localhost:$port/api/exception-test/json-parsing-issue"))
                        .setHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                        .POST(BodyPublishers.ofString("{")
                        ).build(), BodyHandlers.ofString()).body()

        // then
        val errorResponse = objectMapper.readValue(responseBody, ErrorResponse::class.java)
        val responseEntity = ResponseEntity
                .status(errorResponse.status)
                .body(errorResponse)
        assertThat(responseEntity.body).isInstanceOf(ErrorResponse::class.java)
        assertThat(responseEntity.statusCode)
                .isEqualTo(CommonErrorInfo.JSON_PARSING_ERROR.status)
        assertThat(errorResponse.title)
                .isEqualTo(CommonErrorInfo.JSON_PARSING_ERROR.message)
        assertThat(errorResponse.errorCode)
                .isEqualTo(CommonErrorInfo.JSON_PARSING_ERROR.errorCode)
    }

    @Test
    fun shouldChooseNotFoundCode_whenNotExistingEndpointHasBeenCalled() {
        // when
        val response = given()
                .`when`()["/api/exception-test/not-found"]

        // then
        assertErrorInfo(response, CommonErrorInfo.NO_HANDLER_FOUND)
    }

    @Test
    fun shouldChooseValidationErrorCode_whenInvalidPayloadProvided() {
        // when
        val response = given()
                .body(ValidAge(5))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().post("/api/exception-test/validation-error")

        // then
        assertErrorInfo(response, CommonErrorInfo.VALIDATION_ERROR, ErrorDetail("value",
                "Min", "must be greater than or equal to 18"))
    }
}