/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web.errorcode

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import io.restassured.module.mockmvc.response.MockMvcResponse
import org.assertj.core.api.Assertions.assertThat
import org.jbb.swanframework.BaseSwanIT
import org.jbb.swanframework.extractListFromBody
import org.jbb.swanframework.web.CommonErrorInfo
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

internal class ErrorCodeResourceIT : BaseSwanIT() {

    @Test
    fun getAllEndpointShouldReturnAllErrorCodes() {
        // when
        val response: MockMvcResponse = given().contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().get(ERROR_CODES_PATH)

        // then
        response.then().status(HttpStatus.OK)
        val errorCodes = response.extractListFromBody(ErrorCodeDto::class.java)
        assertThat(errorCodes).isNotEmpty
        assertThat(errorCodes)
                .allMatch { (_, errorCode) -> errorCode.isNotBlank() }
        assertThat(errorCodes)
                .allMatch { (httpStatus) -> httpStatus in 400..599 }
        assertThat(errorCodes)
                .allMatch { (_, _, message) -> message.isNotBlank() }
    }

    @Test
    fun getAllEndpointShouldFilterByStatus() {
        // when
        val response: MockMvcResponse = given().contentType(MediaType.APPLICATION_JSON_VALUE)
                .queryParam("status", 500)
                .`when`().get(ERROR_CODES_PATH)

        // then
        response.then().status(HttpStatus.OK)
        val errorCodes = response.extractListFromBody(ErrorCodeDto::class.java)
        assertThat(errorCodes).isNotEmpty
        assertThat(errorCodes)
                .allMatch { (httpStatus) -> httpStatus == 500 }
    }

    @Test
    fun getAllEndpointShouldFilterByPartOfErrorCode() {
        // when
        val response: MockMvcResponse = given().contentType(MediaType.APPLICATION_JSON_VALUE)
                .queryParam("errorCodeContains", "GNRL")
                .`when`().get(ERROR_CODES_PATH)

        // then
        response.then().status(HttpStatus.OK)
        val errorCodes = response.extractListFromBody(ErrorCodeDto::class.java)
        assertThat(errorCodes).isNotEmpty
        assertThat(errorCodes)
                .allMatch { (_, errorCode) -> "GNRL" in errorCode }
    }

    @Test
    fun getSingleErrorCodeShouldWork() {
        // when
        val response: MockMvcResponse = given().contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().get("$ERROR_CODES_PATH/IERR-001")

        // then
        response.then().status(HttpStatus.OK)
        val errorCode = response.`as`(ErrorCodeDto::class.java)
        assertThat(errorCode.errorCode).isEqualTo("IERR-001")
    }

    @Test
    fun shouldReturnErrorWhenErrorCodeNotFound() {
        // when
        val response: MockMvcResponse = given().contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().get("$ERROR_CODES_PATH/ZZZZ-999")

        // then
        assertErrorInfo(response, CommonErrorInfo.ERROR_CODE_NOT_FOUND)
    }
}