/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.eventbus

import com.google.common.eventbus.Subscribe
import org.assertj.core.util.Lists
import javax.validation.constraints.Min

internal class EventHandler : DomainEventBusListener {
    val events: MutableList<DomainEvent> = Lists.newArrayList()

    @Subscribe
    @Suppress("unused")
    fun handle(event: ExampleEvent) {
        events.add(event)
    }

    @Subscribe
    @Suppress("unused")
    fun handle(@Suppress("UNUSED_PARAMETER") event: FailureEvent?) {
        throw MyCustomException()
    }

    class ExampleEvent(@field:Min(0) val value: Int) : DomainEvent()
    class FailureEvent : DomainEvent()
    class MyCustomException : RuntimeException()
}