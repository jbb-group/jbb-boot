/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.startup

import org.assertj.core.api.Assertions.assertThat
import org.jbb.swanframework.BaseSwanIT
import org.jbb.swanframework.JbbSwanFrameworkApplication
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = [JbbSwanFrameworkApplication::class],
        properties = ["jbb.swan-framework.start-up.early-server-start=true"]
)
@AutoConfigureMockMvc
internal class NotReadyErrorIT : BaseSwanIT() {

    @Autowired
    private lateinit var applicationContext: ApplicationContext

    @Test
    fun shouldSetUpStartupBeans_whenEarlyServerStartPropertySetToTrue() {

        val postProcessor = applicationContext.getBean(ProgressBeanPostProcessor::class.java)
        assertThat(postProcessor).isNotNull

        val startUpListener = applicationContext.getBean(StartUpListener::class.java)
        assertThat(startUpListener).isNotNull

        assertThat(StartUpListener.promise).isDone

    }
}