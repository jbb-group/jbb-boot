/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework

import io.restassured.config.EncoderConfig
import io.restassured.module.mockmvc.RestAssuredMockMvc
import io.restassured.module.mockmvc.config.RestAssuredMockMvcConfig
import io.restassured.module.mockmvc.response.MockMvcResponse
import org.assertj.core.api.Assertions.assertThat
import org.jbb.swanframework.web.ErrorDetail
import org.jbb.swanframework.web.ErrorInfo
import org.jbb.swanframework.web.ErrorResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.servlet.MockMvc
import javax.annotation.PostConstruct

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = [JbbSwanFrameworkApplication::class],
        properties = ["jbb.swan-framework.start-up.early-server-start=false"]
)
@AutoConfigureMockMvc
class BaseSwanIT {
    @Autowired
    protected lateinit var mockMvc: MockMvc

    @PostConstruct
    fun injectLocalRandomPort() {
        RestAssuredMockMvc.mockMvc(mockMvc)
        RestAssuredMockMvc.config = RestAssuredMockMvcConfig()
                .encoderConfig(
                        EncoderConfig.encoderConfig()
                                .appendDefaultContentCharsetToContentTypeIfUndefined(false)
                )
    }

    fun assertErrorInfo(
            response: MockMvcResponse, errorInfo: ErrorInfo,
            vararg details: ErrorDetail?
    ) {
        response.then().status(errorInfo.status)

        val errorResponse = response.`as`(ErrorResponse::class.java)
        assertThat(errorResponse.title).isEqualTo(errorInfo.message)
        assertThat(errorResponse.errorCode).isEqualTo(errorInfo.errorCode)
        assertThat(errorResponse.details).containsExactlyInAnyOrder(*details)
    }
}

fun <T> MockMvcResponse.extractListFromBody(clazz: Class<T>) =
        this.then().extract().jsonPath().getList(".", clazz)

fun <T> MockMvcResponse.extractFromBody(clazz: Class<T>) = this.`as`(clazz)