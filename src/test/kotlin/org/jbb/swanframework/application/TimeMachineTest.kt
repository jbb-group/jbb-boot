/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.application

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.time.ZoneOffset

internal class TimeMachineTest {

    @Test
    fun shouldSetNow_whenUseFixedClockUsed() {
        // given
        val exampleInstant = LocalDateTime.of(2021, 2, 28, 23, 27, 11).toInstant(ZoneOffset.UTC)

        // when
        TimeMachine.useFixedClockAt(exampleInstant)
        val now = TimeMachine.now()

        // then
        assertThat(now).isEqualTo(exampleInstant)
    }

    @AfterEach
    fun tearDown() {
        TimeMachine.useSystemDefaultZoneClock()
    }
}
