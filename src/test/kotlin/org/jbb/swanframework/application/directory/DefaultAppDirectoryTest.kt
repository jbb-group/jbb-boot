/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.application.directory

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.util.Files
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.util.FileSystemUtils
import java.nio.file.Path
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class DefaultAppDirectoryTest {
    @Mock
    private lateinit var appDirectoryProperties: AppDirectoryProperties

    @InjectMocks
    private lateinit var defaultAppDirectory: DefaultAppDirectory

    @Test
    fun shouldCreateAppDirectory_ifItIsNotYetCreated() {

        // given
        val path = Path.of(Files.temporaryFolderPath()).resolve("jbb-test-${UUID.randomUUID()}")
        given(appDirectoryProperties.appDirectoryPath()).willReturn(path)

        // when
        val created = defaultAppDirectory.create()

        // then
        assertThat(appDirectoryProperties.appDirectoryPath()).exists()
        assertThat(created).isTrue

        // when calling again
        val secondCreated = defaultAppDirectory.create()

        // then
        assertThat(secondCreated).isFalse

        // clean up
        FileSystemUtils.deleteRecursively(path)
    }
}