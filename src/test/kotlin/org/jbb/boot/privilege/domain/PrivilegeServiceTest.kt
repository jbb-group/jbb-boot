/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.privilege.domain

import com.nhaarman.mockitokotlin2.any
import org.jbb.boot.privilege.api.AdministratorPrivilegeAddedEvent
import org.jbb.boot.privilege.api.AdministratorPrivilegeRemovedEvent
import org.jbb.boot.privilege.api.Privilege
import org.jbb.swanframework.eventbus.DomainEventBus
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean

@SpringBootTest
internal class PrivilegeServiceTest {
    @Autowired
    private lateinit var privilegeService: PrivilegeService

    @Autowired
    private lateinit var repository: PrivilegeRepository

    @MockBean
    private lateinit var domainEventBusMock: DomainEventBus

    @BeforeEach
    fun cleanUpPrivileges() {
        repository.deleteAll()
    }

    @Test
    fun shouldSendAdministratorPrivilegeAddedEvent_whenPrivilegeAdded() {
        // given
        Mockito.reset(domainEventBusMock)
        val memberId = "memberId"

        // when
        privilegeService.updatePrivileges(memberId, setOf(Privilege.ADMINISTRATOR))

        // then
        verify(domainEventBusMock).post(any<AdministratorPrivilegeAddedEvent>())
    }

    @Test
    fun shouldSendAdministratorPrivilegeRemovedEvent_whenPrivilegeRemoved() {
        // given
        Mockito.reset(domainEventBusMock)
        val memberId = "memberId"
        repository.save(PrivilegeEntity(memberId = memberId, privilege = Privilege.ADMINISTRATOR))

        // when
        privilegeService.updatePrivileges(memberId, emptySet())

        // then
        verify(domainEventBusMock).post(any<AdministratorPrivilegeRemovedEvent>())
    }
}