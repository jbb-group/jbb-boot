/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.signin.web

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.ADMIN_PASSWORD
import org.jbb.boot.ADMIN_USERNAME
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.member.api.CreateMemberCommand
import org.jbb.boot.member.api.MemberWriteFacade
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.web.CommonErrorInfo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic

const val API_V1_SIGN_IN = "/api/v1/sign-in?login=%s&password=%s"

@Import(TestEndpointsConfiguration::class)
internal class AuthorizationSecurityConfigurationIT : AbstractBaseResourceIT() {
    @Autowired
    private lateinit var memberWriteFacade: MemberWriteFacade

    @Autowired
    private lateinit var signInEventHandler: SignInEventHandler

    @Test
    fun shouldReturn401_whenEndpointNeedsAuthentication_andNoneAuthorizationProvided() {
        // when
        val response = given()
                .`when`()["/api/test/authenticated"]

        // then
        assertErrorInfo(response, CommonErrorInfo.UNAUTHORIZED)
    }

    @Test
    fun shouldGrantAccess_whenEndpointNeedsAuthentication_andValidBasicAuthProvided() {
        // given
        memberWriteFacade.createMember(
                CreateMemberCommand(
                        username = "test-auth",
                        password = "auth".toCharArray(),
                        email = "auth@test.com"
                )
        ).assertRight()

        // when
        val response = given()
                .auth().with(httpBasic("test-auth", "auth"))
                .`when`()["/api/test/authenticated"]

        // then
        response.then().status(HttpStatus.OK)
    }

    @Test
    fun shouldReturn403_whenEndpointNeedsAdminRole_andRegularUserAuthProvided() {
        // given
        memberWriteFacade.createMember(
                CreateMemberCommand(
                        username = "test-auth2",
                        password = "auth".toCharArray(),
                        email = "auth2@test.com"
                )
        ).assertRight()

        // when
        val response = given()
                .auth().with(httpBasic("test-auth2", "auth"))
                .`when`()["/api/test/admin"]

        // then
        assertErrorInfo(response, CommonErrorInfo.FORBIDDEN)
    }

    @Test
    fun shouldGrantAccess_whenEndpointNeedsAdminRole_andValidAdminAuthProvided() {
        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .`when`()["/api/test/admin"]

        // then
        response.then().status(HttpStatus.OK)
    }

    @Test
    fun shouldReturn401_whenWrongBasicCredentialsProvided() {
        // when
        val response = given()
                .auth().with(httpBasic("aaa", "aaa"))
                .`when`()["/api/test/authenticated"]

        // then
        assertErrorInfo(response, CommonErrorInfo.BAD_CREDENTIALS)
    }

    @Test
    fun shouldSignInAndSignOutWithApiSuccessfully_andSendSignInSuccessEvent() {
        // given
        signInEventHandler.cleanUpSavedEvents()
        val url = String.format(
                API_V1_SIGN_IN,
                ADMIN_USERNAME,
                ADMIN_PASSWORD
        )

        // when
        val signInResponse = given()
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .`when`().post(url)

        // then
        signInResponse.then().status(HttpStatus.NO_CONTENT)
        assertThat(signInEventHandler.successEvents).hasSize(1)

        val event = signInEventHandler.successEvents[0]
        assertThat(event.memberId).isNotEmpty
        assertThat(event.createdSessionId).isNotEmpty

        // when
        val signOutResponse = given()
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .`when`().post("/api/v1/sign-out")

        // then
        signOutResponse.then().status(HttpStatus.NO_CONTENT)
    }

    @Test
    fun shouldFailedWhenInvalidCredentialsProvidedToSignInEndpoint_andSendSignInFailedEvent() {
        // given
        signInEventHandler.cleanUpSavedEvents()
        val url = String.format(API_V1_SIGN_IN, ADMIN_USERNAME, "invalid-pass")

        // when
        val response = given()
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .`when`().post(url)

        // then
        assertErrorInfo(response, CommonErrorInfo.BAD_CREDENTIALS)
    }

    @Test
    fun shouldFailedWithBadCredentials_whenNotExistingUsernameProvided() {
        // given
        signInEventHandler.cleanUpSavedEvents()
        val url = String.format(
                API_V1_SIGN_IN, "not-existing-username",
                "invalid-pass"
        )

        // when
        val response = given()
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .`when`().post(url)

        // then
        assertErrorInfo(response, CommonErrorInfo.BAD_CREDENTIALS)
    }
}