/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.domain

import com.nhaarman.mockitokotlin2.any
import org.jbb.boot.member.api.CreateMemberCommand
import org.jbb.boot.member.api.MemberRegisteredEvent
import org.jbb.boot.member.api.MemberRemovedEvent
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.eventbus.DomainEventBus
import org.junit.jupiter.api.Test
import org.mockito.Mockito.reset
import org.mockito.Mockito.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean

@SpringBootTest
internal class MemberServiceTest {
    @Autowired
    private lateinit var memberService: MemberService

    @MockBean
    private lateinit var domainEventBusMock: DomainEventBus

    @Test
    fun shouldSendMemberRegisteredEvent_whenMemberCreated() {
        // given
        reset(domainEventBusMock)

        // when
        memberService.createMember(validCreateMemberCommand()).assertRight()

        // then
        verify(domainEventBusMock).post(any<MemberRegisteredEvent>())
    }

    @Test
    fun shouldSendMemberRemovedEvent_whenMemberRemoved() {
        // given
        reset(domainEventBusMock)
        val (memberId) = memberService.createMember(validCreateMemberCommand()).assertRight()

        // when
        memberService.deleteMember(memberId).assertRight()

        // then
        verify(domainEventBusMock).post(any<MemberRemovedEvent>())
    }

    private fun validCreateMemberCommand() = CreateMemberCommand(
            username = "test-service",
            email = "email@email.com",
            password = "passw".toCharArray()
    )
}