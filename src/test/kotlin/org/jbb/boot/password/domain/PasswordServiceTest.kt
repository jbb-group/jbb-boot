/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.password.domain

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.member.api.MemberReadFacade
import org.jbb.boot.password.api.ExpirationRules
import org.jbb.boot.password.api.PasswordExpirationReason
import org.jbb.boot.password.api.PasswordPolicy
import org.jbb.boot.password.domain.policy.PasswordPolicyService
import org.jbb.swanframework.application.TimeMachine
import org.jbb.swanframework.dryrun.DryRunContext
import org.jbb.swanframework.eventbus.DomainEventBus
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.security.crypto.password.PasswordEncoder
import java.time.Instant
import java.time.temporal.ChronoUnit

@ExtendWith(MockitoExtension::class)
internal class PasswordServiceTest {

    @InjectMocks
    private lateinit var passwordService: PasswordService

    @Mock
    private lateinit var passwordRepository: PasswordRepository

    @Mock
    private lateinit var passwordEntityFactory: PasswordEntityFactory

    @Mock
    private lateinit var memberReadFacade: MemberReadFacade

    @Mock
    private lateinit var passwordValidatorService: PasswordValidatorService

    @Mock
    private lateinit var passwordPolicyService: PasswordPolicyService

    @Mock
    private lateinit var passwordEncoder: PasswordEncoder

    @Mock
    private lateinit var domainEventBus: DomainEventBus

    @Mock
    private lateinit var dryRunContext: DryRunContext

    @Test
    fun shouldUpdateExpirationStatus_whenSetExpirationStatusCalled() {
        // given
        val entity = PasswordEntity("memberId", "pass", Instant.now(), false)

        given(passwordRepository.findTheNewestByMemberId(any())).willReturn(entity)

        // when
        passwordService.setExpirationStatus("memberId", true)

        // then
        assertThat(entity.explicitExpired).isTrue

        // when
        passwordService.setExpirationStatus("memberId", false)

        // then
        assertThat(entity.explicitExpired).isFalse
    }

    @Test
    fun shouldDoNothing_whenSetExpirationStatusCalledForNotExistingMember() {
        // given
        given(passwordRepository.findTheNewestByMemberId(any())).willReturn(null)

        // when
        passwordService.setExpirationStatus("anyMember", true)

        // then
        // no exception thrown
    }

    @Test
    fun shouldReturnNullPasswordExpiration_whenNoMemberFound() {
        // given
        given(passwordRepository.findTheNewestByMemberId(eq("not-existing"))).willReturn(null)

        // when
        val expirationReason = passwordService.getPasswordExpirationReason("not-existing")

        // then
        assertThat(expirationReason).isNull()
    }

    @Test
    fun shouldReturnExplicitPasswordExpiration_whenExpirationHasBeenSet() {
        // given
        val entity = PasswordEntity("member", "pass", Instant.now(), true)
        given(passwordRepository.findTheNewestByMemberId(eq("member"))).willReturn(entity)

        // when
        val expirationReason = passwordService.getPasswordExpirationReason("member")

        // then
        assertThat(expirationReason).isEqualTo(PasswordExpirationReason.EXPLICIT_EXPIRATION)
    }

    @Test
    fun shouldNotReturnPasswordExpiration_whenExpirationTimeHasNotPassed() {
        // given
        val passwordFrom = Instant.now()
        TimeMachine.useFixedClockAt(passwordFrom.plus(3, ChronoUnit.DAYS))

        val entity = PasswordEntity("member", "pass", passwordFrom, false)
        given(passwordRepository.findTheNewestByMemberId(eq("member"))).willReturn(entity)

        val passwordPolicy = Mockito.mock(PasswordPolicy::class.java)
        given(passwordPolicy.expirationRules).willReturn(ExpirationRules(days = 5))
        given(passwordPolicyService.getPasswordPolicy()).willReturn(passwordPolicy)

        // when
        val expirationReason = passwordService.getPasswordExpirationReason("member")

        // then
        assertThat(expirationReason).isNull()
    }

    @Test
    fun shouldReturnPasswordExpirationDueToPolicy_whenExpirationTimeHasPassed() {
        // given
        val passwordFrom = Instant.now()
        TimeMachine.useFixedClockAt(passwordFrom.plus(3, ChronoUnit.DAYS))

        val entity = PasswordEntity("member", "pass", passwordFrom, false)
        given(passwordRepository.findTheNewestByMemberId(eq("member"))).willReturn(entity)

        val passwordPolicy = Mockito.mock(PasswordPolicy::class.java)
        given(passwordPolicy.expirationRules).willReturn(ExpirationRules(days = 2))
        given(passwordPolicyService.getPasswordPolicy()).willReturn(passwordPolicy)

        // when
        val expirationReason = passwordService.getPasswordExpirationReason("member")

        // then
        assertThat(expirationReason).isEqualTo(PasswordExpirationReason.POLICY)
    }

    @AfterEach
    fun tearDown() {
        TimeMachine.useSystemDefaultZoneClock()
    }
}