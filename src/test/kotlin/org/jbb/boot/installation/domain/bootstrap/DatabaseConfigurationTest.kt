/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.domain.bootstrap

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class DatabaseConfigurationTest {

    private val config = DatabaseConfiguration()

    @Test
    fun shouldGenerateProperJpaProperties_forNoBootstrap() {
        // when
        val prop = config.additionalProperties(null)

        // then
        assertThat(prop).hasSize(2)
        assertThat(prop["hibernate.hbm2ddl.auto"]).isEqualTo("create-only")
        assertThat(prop["hibernate.dialect"]).isEqualTo("org.hibernate.dialect.H2Dialect")
    }

    @Test
    fun shouldGenerateProperJpaProperties_forH2Bootstrap() {
        // when
        val prop =
                config.additionalProperties(Bootstrap(database = H2DatabaseBoostrap("omc", "pass")))

        // then
        assertThat(prop).hasSize(2)
        assertThat(prop["hibernate.hbm2ddl.auto"]).isEqualTo("update")
        assertThat(prop["hibernate.dialect"]).isEqualTo("org.hibernate.dialect.H2Dialect")
    }

    @Test
    fun shouldGenerateProperJpaProperties_forPostgresBootstrap() {
        // when
        val prop = config.additionalProperties(
                Bootstrap(
                        database = PostgresDatabaseBoostrap(
                                host = "",
                                databaseName = "",
                                port = 0,
                                username = "",
                                password = ""
                        )
                )
        )

        // then
        assertThat(prop).hasSize(2)
        assertThat(prop["hibernate.hbm2ddl.auto"]).isEqualTo("update")
        assertThat(prop["hibernate.dialect"]).isEqualTo("org.hibernate.dialect.PostgreSQLDialect")
    }
}