/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.web

import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import org.assertj.core.api.Assertions.assertThat
import org.jbb.boot.AbstractBaseResourceIT
import org.jbb.boot.httpBasic
import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.ErrorDetail
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

const val API_V1_DEFAULT_FORMAT_SETTINGS = "/api/v1/format-settings/default"

internal class DefaultFormatSettingsResourceIT : AbstractBaseResourceIT() {

    @Test
    fun adminShouldBeAbleToUpdateDefaultFormatSettings() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(DefaultFormatSettingsDto("dd.MM.yyyy HH:mm:ss", "HH:mm:ss"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_DEFAULT_FORMAT_SETTINGS, testMember.memberId)

        // then
        response.then().status(HttpStatus.OK)

        // when
        val getResponse = given()
                .`when`()[API_V1_DEFAULT_FORMAT_SETTINGS]

        // then
        val (dateTimeFormat, durationFormat) = getResponse.`as`(DefaultFormatSettingsDto::class.java)
        assertThat(dateTimeFormat).isEqualTo("dd.MM.yyyy HH:mm:ss")
        assertThat(durationFormat).isEqualTo("HH:mm:ss")
    }

    @Test
    fun shouldFailed_whenNotValidCustomDateTimeFormatProvided() {
        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(DefaultFormatSettingsDto("dd.MM.yyyyTrh1 HH:mm:ss", "HH:mm:ss"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_DEFAULT_FORMAT_SETTINGS)

        // then
        assertErrorInfo(response, FormatErrorInfo.DEFAULT_FORMAT_SETTINGS_UPDATE_FAILED,
                ErrorDetail("dateTimeFormat", null, "date time format is not valid"))
    }

    @Test
    fun shouldFailed_whenNotValidCustomDurationFormatProvided() {
        // when
        val response = given()
                .auth().with(adminHttpBasic)
                .body(DefaultFormatSettingsDto("dd.MM.yyyy HH:mm:ss", "HTrh1:mm:ss"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_DEFAULT_FORMAT_SETTINGS)

        // then
        assertErrorInfo(response, FormatErrorInfo.DEFAULT_FORMAT_SETTINGS_UPDATE_FAILED,
                ErrorDetail("durationFormat", null, "duration format is not valid"))
    }

    @Test
    fun simpleUserShouldNotBeAbleToUseThatEndpointForChangingFormatSettings() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .auth().with(testMember.httpBasic())
                .body(DefaultFormatSettingsDto("dd.MM.yyyy HH:mm:ss", "HH:mm:ss"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_DEFAULT_FORMAT_SETTINGS, testMember.memberId)

        // then
        assertErrorInfo(response, CommonErrorInfo.FORBIDDEN)
    }

    @Test
    fun notLoggedUserShouldNotBeAbleToUseThatEndpointForChangingFormatSettings() {
        // given
        val testMember = testMemberHelper.createMember()

        // when
        val response = given()
                .body(DefaultFormatSettingsDto("dd.MM.yyyy HH:mm:ss", "HH:mm:ss"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .`when`().put(API_V1_DEFAULT_FORMAT_SETTINGS, testMember.memberId)

        // then
        assertErrorInfo(response, CommonErrorInfo.UNAUTHORIZED)
    }

}