/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.domain.bootstrap

import arrow.core.Either
import com.fasterxml.jackson.databind.ObjectMapper
import com.github.zafarkhaja.semver.Version
import org.jbb.boot.VERSION_0_0_0
import org.jbb.boot.installation.api.*
import org.jbb.boot.installation.domain.AUTO_INSTALL_TMP_FILE
import org.jbb.swanframework.application.RestartManager
import org.jbb.swanframework.application.directory.AppDirectory
import org.springframework.stereotype.Component

internal const val BOOTSTRAP_FILE_NAME = "bootstrap.json"

@Component
internal class BootstrapFileCreator(
        private val appDirectory: AppDirectory,
        private val contextGenerator: InstallationContextGenerator<*>,
        private val objectMapper: ObjectMapper,
        private val restartManager: RestartManager,
) : InstallationAction<DatabaseDetails> {

    override fun name(): InstallationActionName = CREATE_BOOTSTRAP_FILE_ACTION_NAME

    override fun fromVersion(): Version = VERSION_0_0_0

    override fun install(context: DatabaseDetails?):
            Either<InstallationAbortedDueToActionRequired, Unit> {
        val bootstrap = context?.toBootstrap()
                ?: return Either.Left(InstallationAbortedDueToActionRequired("Missing database details"))
        val bootstrapFile = appDirectory.configPath().resolve(BOOTSTRAP_FILE_NAME).toFile()
        if (!bootstrapFile.exists()) {
            bootstrapFile.createNewFile()
            bootstrapFile.writeText(objectMapper.writeValueAsString(bootstrap))

            contextGenerator.getLastProcessedContext()?.let {
                val tempFile = appDirectory.configPath().resolve(AUTO_INSTALL_TMP_FILE).toFile()
                tempFile.createNewFile()
                tempFile.writeText(objectMapper.writeValueAsString(it))
            }

            restartManager.restartApp()
            return Either.Left(InstallationAbortedDueToActionRequired("No longer installation without bootstrap file necessary. Ignore it"))
        }
        return Either.Right(Unit)
    }
}

private fun DatabaseDetails.toBootstrap(): Bootstrap =
        when (this) {
            is H2DatabaseDetails -> Bootstrap(
                    database = H2DatabaseBoostrap(
                            username = this.username,
                            password = this.password
                    )
            )
            is PostgresDatabaseDetails -> Bootstrap(
                    database = PostgresDatabaseBoostrap(
                            host = this.host,
                            port = this.port,
                            databaseName = this.databaseName,
                            username = this.username,
                            password = this.password
                    )
            )
        }
