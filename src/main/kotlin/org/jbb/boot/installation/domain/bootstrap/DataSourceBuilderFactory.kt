/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.domain.bootstrap

import org.jbb.swanframework.application.directory.AppDirectory
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.stereotype.Component

@Component
internal class DataSourceBuilderFactory(private val appDirectory: AppDirectory) {

    fun getDataSourceBuilder(bootstrap: Bootstrap): DataSourceBuilder<*> =
            DataSourceBuilder.create()
                    .driverClassName(bootstrap.database.driverClassName())
                    .url(
                            when (val db = bootstrap.database) {
                                is H2DatabaseBoostrap -> db.url(
                                        appDirectory.path().resolve("database").toString()
                                )
                                is PostgresDatabaseBoostrap -> db.url()
                            }
                    )
                    .username(bootstrap.database.username)
                    .password(bootstrap.database.password)

}