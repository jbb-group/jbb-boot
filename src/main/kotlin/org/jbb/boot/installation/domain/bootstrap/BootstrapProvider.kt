/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.domain.bootstrap

import com.fasterxml.jackson.databind.ObjectMapper
import org.jbb.swanframework.application.directory.AppDirectory
import org.springframework.stereotype.Component

@Component
internal class BootstrapProvider(
        private val appDirectory: AppDirectory,
        private val objectMapper: ObjectMapper
) {

    fun getBootstrap(): Bootstrap? =
            appDirectory.configPath().resolve(BOOTSTRAP_FILE_NAME).toFile()
                    .takeIf { it.exists() }
                    ?.let { objectMapper.readValue(it.readText(), Bootstrap::class.java) }

}