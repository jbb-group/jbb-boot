/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.installation.domain

import org.jbb.swanframework.jpa.BaseEntity
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "JBB_INSTALLATION_LOGS")
internal data class InstallationLogEntity(
        @field:Column(name = "app_version")
        val appVersion: String
) : BaseEntity()