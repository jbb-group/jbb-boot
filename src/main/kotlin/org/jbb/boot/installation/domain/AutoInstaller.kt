/*
 * Copyright (C) 2022 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.installation.domain

import com.fasterxml.jackson.databind.ObjectMapper
import org.jbb.boot.installation.api.AlreadyInstalled
import org.jbb.boot.installation.api.InstallationContextGenerator
import org.jbb.boot.installation.api.InstallationFacade
import org.jbb.swanframework.application.directory.AppDirectory
import org.jbb.swanframework.arrow.assertRight
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import java.nio.file.Files
import java.nio.file.Path

internal const val AUTO_INSTALL_TMP_FILE = "auto-install.json.tmp"

@Component
internal class AutoInstaller(
        private val appDirectory: AppDirectory,
        private val contextGenerator: InstallationContextGenerator<*>,
        private val installationFacade: InstallationFacade,
        private val objectMapper: ObjectMapper
) : ApplicationListener<ApplicationReadyEvent> {

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        installIfPossible()
    }

    fun installIfPossible() {
        try {
            if (autoInstallIfPossible(getAutoInstallTmpPath(), true)
                    || autoInstallIfPossible(getAutoInstallPath())) {
                return
            }
            installationFacade.install(emptyMap()).assertRight()
        } catch (ex: AlreadyInstalled) {

        }
    }

    private fun getAutoInstallPath() = appDirectory.configPath().resolve("auto-install.json")

    private fun getAutoInstallTmpPath() = appDirectory.configPath().resolve(AUTO_INSTALL_TMP_FILE)

    private fun autoInstallIfPossible(
            autoInstallFile: Path,
            removeAfterInstall: Boolean = false
    ): Boolean =
            (autoInstallFile.takeIf { Files.exists(it) }
                    ?.let { Files.readString(it) }
                    ?.let {
                        doARequest(it)

                    } ?: false)
                    .also {
                        if (removeAfterInstall) {
                            autoInstallFile.toFile().delete()
                        }
                    }

    @Suppress("UNCHECKED_CAST")
    private fun doARequest(requestBody: String): Boolean {
        val data = objectMapper.readValue(requestBody, contextGenerator.type())
        val ctx = (contextGenerator as InstallationContextGenerator<Any>).buildContext(data!!)
        installationFacade.install(ctx).assertRight()
        return true
    }
}