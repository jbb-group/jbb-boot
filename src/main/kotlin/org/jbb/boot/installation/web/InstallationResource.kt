/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.installation.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.installation.api.AlreadyInstalled
import org.jbb.boot.installation.api.InstallationFacade
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.installation.AvailableBeforeInstallation
import org.jbb.swanframework.installation.InstallationVerifier
import org.jbb.swanframework.security.SecurityConstants
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

const val INSTALLATION = "/installation"

@RestController
@Tag(name = "Installation API")
@RequestMapping(API_V1 + INSTALLATION, produces = [MediaType.APPLICATION_JSON_VALUE])
internal class InstallationResource(
        private val contextGenerator: DtoInstallationContextGenerator,
        private val installationFacade: InstallationFacade,
        private val installationVerifier: InstallationVerifier,
) {

    @AvailableBeforeInstallation
    @PreAuthorize(SecurityConstants.PERMIT_ALL)
    @Operation(summary = "Perform installation", description = "Performs application installation")
    @PostMapping
    @Throws(AlreadyInstalled::class)
    fun performInstallation(@Validated @RequestBody installationRequestDto: InstallationRequestDto) =
            installationFacade.install(contextGenerator.buildContext(installationRequestDto))
                    .assertRight()

    @AvailableBeforeInstallation
    @PreAuthorize(SecurityConstants.PERMIT_ALL)
    @Operation(
            summary = "Check installation", description = "Checks application installation status",
            responses = [
                ApiResponse(
                        description = "Application is installed", responseCode = "200",
                        content = [
                            Content(
                                    mediaType = "application/json",
                                    schema = Schema(implementation = InstallationDto::class)
                            )]
                ),
                ApiResponse(
                        description = "Application is not installed", responseCode = "404",
                        content = [Content(
                                mediaType = "application/json",
                                schema = Schema(implementation = InstallationDto::class)
                        )]
                )
            ]
    )
    @GetMapping
    fun getInstallationStatus(): ResponseEntity<InstallationDto> {
        return if (installationVerifier.isInstalled()) {
            ResponseEntity.ok(InstallationDto(true))
        } else {
            ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(InstallationDto(false))
        }
    }

}