/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.board.domain

import org.hibernate.validator.constraints.Length
import org.jbb.swanframework.jpa.BaseEntity
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Lob
import javax.persistence.Table
import javax.validation.constraints.NotBlank

@Entity
@Table(name = "JBB_BOARD_SETTINGS")
internal data class BoardSettingsEntity(

        @field:NotBlank
        @field:Length(min = 1, max = 255)
        @field:Column(name = "board_name", nullable = false)
        var boardName: String,

        @field:Lob
        @field:Column(name = "board_description", nullable = false)
        var boardDescription: String,

        @field:Lob
        @field:Column(name = "welcome_message", nullable = false)
        var welcomeMessage: String,
) : BaseEntity()