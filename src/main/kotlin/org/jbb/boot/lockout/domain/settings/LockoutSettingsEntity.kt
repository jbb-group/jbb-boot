/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.lockout.domain.settings

import org.jbb.swanframework.jpa.BaseEntity
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

@Entity
@Table(name = "JBB_LOCKOUT_SETTINGS")
internal data class LockoutSettingsEntity(

        @field:Min(1) @field:Column(name = "attempts_limit")
        var attemptsLimit: Int,

        @field:Min(1) @field:Column(name = "ignore_attempts_from_minutes")
        var ignoreAttemptsFromMinutes: Long,

        @field:Min(1) @field:Column(name = "lock_duration_minutes")
        var lockDurationMinutes: Long,

        @field:NotNull @field:Column(name = "locking_enabled")
        var lockingEnabled: Boolean
) : BaseEntity()