/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.password.web

import org.jbb.boot.password.api.ChangePasswordFailed
import org.jbb.boot.password.api.ChangePasswordPolicyFailed
import org.jbb.boot.password.api.InvalidCurrentPassword
import org.jbb.swanframework.web.ErrorInfo
import org.springframework.http.HttpStatus

internal enum class PasswordErrorInfo(
        override val status: HttpStatus,
        override val errorCode: String,
        override val message: String,
        override val domainExceptionClasses: Set<Class<out Exception>>,
) : ErrorInfo {
    INVALID_PASSWORD_FOR_CURRENT_MEMBER(
            HttpStatus.FORBIDDEN,
            "PASS-100",
            "Provided password for current member is not correct",
            setOf(InvalidCurrentPassword::class.java)
    ),
    PASSWORD_UPDATE_FAILED(
            HttpStatus.BAD_REQUEST, "PASS-101", "Password update failed",
            setOf(ChangePasswordFailed::class.java)
    ),
    PASSWORD_POLICY_UPDATE_FAILED(
            HttpStatus.BAD_REQUEST, "PASS-102", "Password policy update failed",
            setOf(ChangePasswordPolicyFailed::class.java)
    );

}