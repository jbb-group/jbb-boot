/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.web.policy

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.password.api.PasswordBlacklist
import org.jbb.boot.password.api.PasswordPolicyFacade
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AN_ADMINISTRATOR
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = PASSWORD_POLICIES_API)
@RequestMapping(
        value = [API_V1 + PASSWORD_BLACKLIST],
        produces = [MediaType.APPLICATION_JSON_VALUE]
)
internal class PasswordBlacklistResource(private val passwordPolicyFacade: PasswordPolicyFacade) {

    @GetMapping
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @Operation(
            summary = "Get password blacklist",
            description = "Gets a password blacklist"
    )
    fun getPasswordBlacklist(): PasswordBlacklistDto = PasswordBlacklistDto(
            matchBackwards = passwordPolicyFacade.getPasswordPolicy().forbiddenRules.blacklistMatchBackwards,
            passwords = passwordPolicyFacade.getBlacklist().passwords
    )

    @DryRunnable
    @PutMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @Operation(
            summary = "Update password blacklist",
            description = "Updates a password blacklist"
    )
    fun updatePasswordBlacklist(@RequestBody blacklistDto: PasswordBlacklistDto): PasswordBlacklistDto {
        val currentPolicy = passwordPolicyFacade.getPasswordPolicy()
        val updatedPolicy = currentPolicy.copy(
                forbiddenRules = currentPolicy.forbiddenRules
                        .copy(blacklistMatchBackwards = blacklistDto.matchBackwards)
        )
        passwordPolicyFacade.changePasswordPolicy(updatedPolicy).assertRight()
        passwordPolicyFacade.putBlacklist(PasswordBlacklist(blacklistDto.passwords))
        return getPasswordBlacklist()
    }
}