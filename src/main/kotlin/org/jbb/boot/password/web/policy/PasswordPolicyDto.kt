/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.web.policy

import io.swagger.v3.oas.annotations.media.Schema

@Schema(name = "PasswordPolicy")
internal data class PasswordPolicyDto(
        val length: PasswordLength,
        val requiredCharacterTypes: Int,
        val requiredCharacters: RequiredCharacters,
        val rejectUsername: Boolean,
        val rejectLastPasswords: Int,
        val useBlacklist: Boolean,
        val expireAfterDays: Int?,
)

@Schema(name = "PasswordLength")
internal data class PasswordLength(
        val min: Int,
        val max: Int?
)

@Schema(name = "RequiredCharacters")
internal data class RequiredCharacters(
        val lowerCase: Int = 0,
        val upperCase: Int = 0,
        val digit: Int = 0,
        val special: Int = 0,
)