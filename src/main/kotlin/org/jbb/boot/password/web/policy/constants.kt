/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.web.policy

const val PASSWORD_POLICIES_API = "Password policies API"

const val PASSWORD_POLICY = "/password-policy"
const val PASSWORD_BLACKLIST = "/password-blacklist"