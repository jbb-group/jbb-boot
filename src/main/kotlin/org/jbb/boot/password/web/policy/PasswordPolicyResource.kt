/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.web.policy

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.password.api.ChangePasswordPolicyFailed
import org.jbb.boot.password.api.PasswordPolicyFacade
import org.jbb.boot.password.web.policy.PasswordPolicyWebTranslator.toDto
import org.jbb.boot.password.web.policy.PasswordPolicyWebTranslator.toModel
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AN_ADMINISTRATOR
import org.jbb.swanframework.security.SecurityConstants.PERMIT_ALL
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = PASSWORD_POLICIES_API)
@RequestMapping(
        value = [API_V1 + PASSWORD_POLICY],
        produces = [MediaType.APPLICATION_JSON_VALUE]
)
internal class PasswordPolicyResource(
        private val passwordPolicyFacade: PasswordPolicyFacade,
) {

    @GetMapping
    @PreAuthorize(PERMIT_ALL)
    @Operation(
            summary = "Get password policy",
            description = "Gets a password policy"
    )
    fun getPasswordPolicy(): PasswordPolicyDto =
            toDto(passwordPolicyFacade.getPasswordPolicy())

    @DryRunnable
    @PutMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @Operation(
            summary = "Update password policy",
            description = "Updates a password policy"
    )
    @Throws(ChangePasswordPolicyFailed::class)
    fun updatePasswordPolicy(@RequestBody passwordPolicyDto: PasswordPolicyDto): PasswordPolicyDto {
        val updatedPolicy = toModel(
                dto = passwordPolicyDto,
                blacklistMatchBackwards = passwordPolicyFacade
                        .getPasswordPolicy().forbiddenRules.blacklistMatchBackwards
        )
        return toDto(passwordPolicyFacade.changePasswordPolicy(updatedPolicy).assertRight())
    }
}