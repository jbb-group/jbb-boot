/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.domain

import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.jpa.BaseEntity
import java.time.Instant
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "JBB_PASSWORDS")
internal data class PasswordEntity(
        @field:Column(name = "member_id")
        val memberId: MemberId,

        @field:Column(name = "password")
        val password: String,

        @field:Column(name = "applicable_since")
        val applicableSince: Instant,

        @field:Column(name = "explicit_expired")
        var explicitExpired: Boolean
) : BaseEntity()