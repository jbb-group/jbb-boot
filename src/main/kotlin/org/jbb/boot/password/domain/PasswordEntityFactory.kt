/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.domain

import org.jbb.swanframework.application.MemberId
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import java.time.Instant

@Component
internal class PasswordEntityFactory(private val passwordEncoder: PasswordEncoder) {

    fun create(memberId: MemberId, newPassword: CharArray) = PasswordEntity(
            memberId = memberId,
            password = passwordEncoder.encode(String(newPassword)),
            applicableSince = Instant.now(),
            explicitExpired = false
    )

}