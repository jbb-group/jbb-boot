/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.password.domain

import arrow.core.Either
import org.jbb.boot.member.api.MemberReadFacade
import org.jbb.boot.password.api.*
import org.jbb.boot.password.domain.policy.PasswordPolicyService
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.application.TimeMachine
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunContext
import org.jbb.swanframework.eventbus.DomainEventBus
import org.jbb.swanframework.validation.ValidationIssue
import org.springframework.context.annotation.Lazy
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.time.temporal.ChronoUnit

@Service
internal class PasswordService(
        private val passwordRepository: PasswordRepository,
        private val passwordEntityFactory: PasswordEntityFactory,
        @Lazy private val memberReadFacade: MemberReadFacade,
        private val passwordValidatorService: PasswordValidatorService,
        private val passwordPolicyService: PasswordPolicyService,
        private val passwordEncoder: PasswordEncoder,
        private val domainEventBus: DomainEventBus,
        private val dryRunContext: DryRunContext
) : MemberPasswordFacade {

    override fun changePassword(
            memberId: MemberId,
            newPassword: CharArray
    ): Either<ChangePasswordFailed, Unit> {
        val passwordEntity = passwordEntityFactory.create(memberId, newPassword)
        val issues = passwordValidatorService.validateNewPasswordForMember(
                memberId.toMemberData(),
                newPassword
        ).map { it.toValidationIssue("newPassword") }
        if (issues.isNotEmpty()) {
            return Either.Left(ChangePasswordFailed(issues))
        }
        dryRunContext.completeIfDryRun()
        passwordRepository.save(passwordEntity)
        if (passwordRepository.countByMemberId(memberId) > 1) {
            domainEventBus.post(PasswordChangedEvent(memberId))
        }
        return Either.Right(Unit)
    }

    override fun setExpirationStatus(memberId: MemberId, expired: Boolean) {
        dryRunContext.completeIfDryRun()
        passwordRepository.findTheNewestByMemberId(memberId)?.run {
            this.explicitExpired = expired
            passwordRepository.save(this)
            domainEventBus.post(PasswordExpirationSetEvent(this.memberId, expired))
        }
    }

    override fun verifyMemberPassword(memberId: MemberId, password: CharArray):
            Either<InvalidCurrentPassword, Unit> {
        val entity = passwordRepository.findTheNewestByMemberId(memberId)
                ?: throw InvalidCurrentPassword()
        if (!passwordEncoder.matches(password.concatToString(), entity.password)) {
            return Either.Left(InvalidCurrentPassword())
        }
        return Either.Right(Unit)
    }

    override fun getPasswordExpirationReason(memberId: MemberId): PasswordExpirationReason? {
        val entity = passwordRepository.findTheNewestByMemberId(memberId) ?: return null
        if (entity.explicitExpired) return PasswordExpirationReason.EXPLICIT_EXPIRATION
        return passwordPolicyService.getPasswordPolicy().expirationRules.days
                ?.let { entity.applicableSince.plus(it.toLong(), ChronoUnit.DAYS) }
                ?.takeIf { TimeMachine.now().isAfter(it) }
                ?.let { PasswordExpirationReason.POLICY }
    }

    override fun validatePasswordForNewMember(password: CharArray): List<ValidationIssue> =
            passwordValidatorService.validatePasswordForNewMember(password)
                    .map { it.toValidationIssue() }

    override fun getPasswordHash(memberId: MemberId): String? =
            passwordRepository.findTheNewestByMemberId(memberId)?.password

    private fun MemberId.toMemberData() = MemberData(
            memberId = this,
            username = memberReadFacade.getMemberById(this).assertRight().username
    )

}

private fun PolicyError.toValidationIssue(propertyName: String = "password"): ValidationIssue =
        ValidationIssue(
                name = propertyName,
                errorCode = this.javaClass.simpleName,
                message = when (this) {
                    is LastPasswordDetected -> "Password matches one of ${this.lastPasswords} previous passwords"
                    is WordFromBlackListDetected -> "Password contains word '${this.word}' from black list"
                    is ReversedWordFromBlackListDetected -> "Password contains word '${this.word}' from black list in reversed form"
                    is InsufficientAmountOfCharacterTypes -> "Password requires ${this.requiredTypes} types of characters"
                    is InsufficientDigit -> "Password must contain ${this.required} or more digit characters"
                    is InsufficientLowerCase -> "Password must contain ${this.required} or more lowercase characters"
                    is InsufficientSpecial -> "Password must contain ${this.required} or more special characters"
                    is InsufficientUpperCase -> "Password must contain ${this.required} or more uppercase characters"
                    is UsernameDetected -> "Password must not contain a username"
                    is TooLong -> "Password must be no more than ${this.max} characters in length"
                    is TooShort -> "Password must be ${this.min} or more characters in length"
                }
        )
