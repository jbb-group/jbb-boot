/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.web.me

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.member.web.ME_API
import org.jbb.swanframework.security.SecurityConstants.PERMIT_ALL
import org.jbb.swanframework.security.password.ExpiredPasswordIgnored
import org.jbb.swanframework.web.RestConstants.API_V1
import org.jbb.swanframework.web.RestConstants.ME
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@Tag(name = ME_API)
@RequestMapping(API_V1 + ME, produces = [MediaType.APPLICATION_JSON_VALUE])
internal class MeResource(private val meDataProvider: MeDataProvider) {

    @ExpiredPasswordIgnored
    @Operation(summary = "Get details about me", description = "Gets info about current member")
    @PreAuthorize(PERMIT_ALL)
    @GetMapping
    fun getMeInfo(): MeDataDto = meDataProvider.getMeData()

}