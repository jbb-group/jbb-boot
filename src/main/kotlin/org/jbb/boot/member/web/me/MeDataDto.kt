/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.web.me

import io.swagger.v3.oas.annotations.media.Schema


@Schema(name = "MeData")
internal data class MeDataDto(
        val sessionId: String?,
        val currentMember: MemberContextDto?
)