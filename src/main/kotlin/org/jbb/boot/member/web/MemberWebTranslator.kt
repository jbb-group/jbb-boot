/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.member.web

import org.jbb.boot.member.api.CreateMemberCommand
import org.jbb.boot.member.api.Member
import org.jbb.boot.member.api.PublicMember

internal object MemberWebTranslator {

    fun toDto(member: Member): MemberDto = MemberDto(
            memberId = member.memberId,
            username = member.username,
            email = member.email,
            joinedAt = member.joinedAt
    )

    fun toPublicDto(member: PublicMember): PublicMemberDto = PublicMemberDto(
            memberId = member.memberId,
            username = member.username,
            joinedAt = member.joinedAt
    )

    fun toCreateCommand(createMemberDto: CreateMemberDto): CreateMemberCommand =
            CreateMemberCommand(
                    username = createMemberDto.username ?: "",
                    email = createMemberDto.email ?: "",
                    password = createMemberDto.password?.toCharArray() ?: CharArray(0)
            )
}