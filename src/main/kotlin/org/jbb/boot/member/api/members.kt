/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.member.api

import arrow.core.Either
import org.hibernate.validator.constraints.Length
import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.eventbus.DomainEvent
import org.jbb.swanframework.pagination.CursorPage
import org.jbb.swanframework.validation.ValidationFailed
import org.jbb.swanframework.validation.ValidationIssue
import java.time.Instant
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank

// Operations
interface MemberReadFacade {

    fun getMemberById(memberId: MemberId): Either<MemberNotFound, Member>

    fun findMembers(limit: Int, joinBefore: Instant?): CursorPage<PublicMember, Instant>
}

interface MemberWriteFacade {

    fun createMember(command: CreateMemberCommand): Either<MemberCreationFailed, Member>

    fun deleteMember(memberId: MemberId): Either<MemberNotFound, Unit>

    fun changeEmail(memberId: MemberId, newEmail: String): Either<MemberUpdateFailed, Member>
}

// Errors
sealed class MemberUpdateFailed : RuntimeException() {
    class MemberNotFound(val memberId: MemberId) : MemberUpdateFailed()

    class ChangeEmailFailed(override val issues: List<ValidationIssue>) : MemberUpdateFailed(),
            ValidationFailed

}

class MemberNotFound(val memberId: MemberId) : RuntimeException()

class MemberCreationFailed(override val issues: List<ValidationIssue>) : RuntimeException(),
        ValidationFailed

// Models
data class Member(
        val memberId: MemberId,
        @field:NotBlank @field:Length(min = 4, max = 32) val username: String,
        @field:Email @field:NotBlank val email: String,
        val joinedAt: Instant,
)

data class PublicMember(
        val memberId: MemberId,
        val username: String,
        val joinedAt: Instant,
)

data class CreateMemberCommand(
        val username: String,
        val email: String,
        val password: CharArray
)

// Events
class MemberRegisteredEvent(val memberId: MemberId) : DomainEvent() {
    override fun toString(): String {
        return "MemberRegisteredEvent(memberId='$memberId') ${super.toString()}"
    }
}

class MemberRemovedEvent(val memberId: MemberId) : DomainEvent() {
    override fun toString(): String {
        return "MemberRemovedEvent(memberId='$memberId') ${super.toString()}"
    }
}

class EmailChangedEvent(val memberId: MemberId) : DomainEvent() {
    override fun toString(): String {
        return "EmailChangedEvent(memberId='$memberId') ${super.toString()}"
    }
}