/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.category.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.category.api.*
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AN_ADMINISTRATOR
import org.jbb.swanframework.security.SecurityConstants.PERMIT_ALL
import org.jbb.swanframework.web.RestConstants.API_V1
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = CATEGORIES_API)
@RequestMapping(API_V1 + CATEGORIES, produces = [MediaType.APPLICATION_JSON_VALUE])
internal class CategoryResource(
        private val categoryReadFacade: CategoryReadFacade,
        private val categoryWriteFacade: CategoryWriteFacade,
) {

    @GetMapping
    @PreAuthorize(PERMIT_ALL)
    @Operation(
            summary = "Get all categories (as a flat list)",
            description = "Gets all categories in Depth-first search order"
    )
    fun getCategories(
            @RequestParam(name = "slug", required = false) slug: String?
    ): List<CategoryDto> =
            slug?.let { listOfNotNull(categoryReadFacade.getCategoryBySlug(it)?.toDto()) }
                    ?: categoryReadFacade.getCategoriesDFS().map { it.toDto() }

    @GetMapping(CATEGORY_ID)
    @PreAuthorize(PERMIT_ALL)
    @Operation(
            summary = "Get a category",
            description = "Gets a category"
    )
    @Throws(CategoryNotFound::class)
    fun getCategory(@PathVariable(CATEGORY_ID_VAR) categoryId: CategoryId): CategoryDto =
            categoryReadFacade.getCategory(categoryId)?.toDto()
                    ?: throw CategoryNotFound(categoryId)

    @DryRunnable
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @Operation(
            summary = "Create a new category",
            description = "Creates a new category"
    )
    @Throws(CategoryValidationFailed::class)
    fun createCategory(@RequestBody createCategoryRequestDto: CreateCategoryRequestDto): CategoryDto =
            categoryWriteFacade.createCategory(createCategoryRequestDto.toModel()).assertRight()
                    .toDto()

    @DryRunnable
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @PutMapping(path = [CATEGORY_ID + DETAILS], consumes = [MediaType.APPLICATION_JSON_VALUE])
    @Operation(
            summary = "Update details for a category",
            description = "Updates details for a category"
    )
    @Throws(
            CategoryUpdateFailed.CategoryNotFound::class,
            CategoryUpdateFailed.CategoryValidationFailed::class
    )
    fun updateCategoryDetails(
            @PathVariable(CATEGORY_ID_VAR) categoryId: CategoryId,
            @RequestBody categoryDetailsDto: CategoryDetailsDto
    ): CategoryDetailsDto =
            categoryWriteFacade.updateCategoryDetails(categoryId, categoryDetailsDto.toModel())
                    .assertRight().toDto().details

    @DryRunnable
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @PutMapping(path = [CATEGORY_ID + LOCATION], consumes = [MediaType.APPLICATION_JSON_VALUE])
    @Operation(
            summary = "Update location for a category",
            description = "Updates location for a category"
    )
    @Throws(CategoryNotFound::class, CategoryValidationFailed::class)
    fun updateCategoryLocation(
            @PathVariable(CATEGORY_ID_VAR) categoryId: CategoryId,
            @RequestBody categoryLocationDto: CategoryLocationDto
    ): CategoryLocationDto =
            categoryWriteFacade.updateCategoryLocation(categoryId, categoryLocationDto.toModel())
                    .assertRight().toDto().location

    @DryRunnable
    @PreAuthorize(IS_AN_ADMINISTRATOR)
    @DeleteMapping(path = [CATEGORY_ID])
    @Operation(
            summary = "Delete a category",
            description = "Removes a category"
    )
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Throws(CategoryNotFound::class)
    fun deleteCategory(
            @PathVariable(CATEGORY_ID_VAR) categoryId: CategoryId,
            @RequestParam(
                    name = "cascade",
                    required = false,
                    defaultValue = "false"
            ) cascade: Boolean
    ) {
        categoryWriteFacade.deleteCategory(categoryId, cascade).assertRight()
    }
}