/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.category.domain

import org.jbb.boot.category.api.Category
import org.jbb.boot.category.api.CategoryId
import org.jbb.swanframework.validation.ValidationIssue
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component
internal class ParentCategoryValidator(private val categoryRepository: CategoryRepository) {

    fun validate(parentCategoryId: CategoryId?, inContextOfCategory: Category?): ValidationIssue? {
        val parentCategory = parentCategoryId?.let {
            categoryRepository.findByIdOrNull(it) ?: return issue("Parent category does not exist")
        }
        if (parentCategory != null && parentCategoryId == inContextOfCategory?.categoryId) {
            return issue("Category cannot be its own parent")
        }
        if (parentCategory?.parentCategory != null) {
            return issue("Parent cannot have a parent")
        }
        if (inContextOfCategory?.childCategoryIds?.isNotEmpty() == true
                && parentCategoryId != null) {
            return issue("Cannot change top category to subcategory when it contains any child")
        }
        return null
    }

    private fun issue(message: String) = ValidationIssue(
            name = "parentCategoryId",
            errorCode = null,
            message = message
    )
}