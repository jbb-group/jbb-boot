/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.category.domain

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
internal interface CategoryRepository : PagingAndSortingRepository<CategoryEntity, String> {
    fun findAllBySlug(slug: String): CategoryEntity?

    @Query("select c from CategoryEntity c where c.parentCategory = null order by c.position")
    fun findTopCategories(): List<CategoryEntity>
}