/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.category.domain

import org.jbb.boot.category.api.CategoryId
import org.jbb.swanframework.validation.ValidationIssue
import org.springframework.stereotype.Component

@Component
internal class SlugUniquenessValidator(private val categoryRepository: CategoryRepository) {

    fun validate(slug: String, inContextOfCategoryId: CategoryId? = null): ValidationIssue? {
        return categoryRepository.findAllBySlug(slug)
                ?.takeIf { it.id != inContextOfCategoryId }
                ?.let {
                    ValidationIssue(
                            "slug",
                            "slug-already-exists",
                            "Slug is already used in different category"
                    )
                }
    }
}