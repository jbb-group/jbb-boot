/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.category.domain

import arrow.core.Either
import org.jbb.boot.category.api.*
import org.jbb.swanframework.dryrun.DryRunContext
import org.jbb.swanframework.eventbus.DomainEventBus
import org.jbb.swanframework.validation.ValidationService
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
internal class CategoryService(
        private val repository: CategoryRepository,
        private val validationService: ValidationService,
        private val categoryFactory: CategoryFactory,
        private val parentCategoryValidator: ParentCategoryValidator,
        private val slugUniquenessValidator: SlugUniquenessValidator,
        private val positionValidator: PositionValidator,
        private val dryRunContext: DryRunContext,
        private val domainEventBus: DomainEventBus,
) : CategoryReadFacade, CategoryWriteFacade {

    @Transactional
    override fun createCategory(command: CreateCategoryCommand): Either<CategoryValidationFailed, Category> {
        val issues = validationService.validate(command).toMutableList()
        command.parentCategoryId
                ?.let { parentCategoryValidator.validate(it, null) }
                ?.let { issues.add(it) }
        slugUniquenessValidator.validate(command.details.slug)
                ?.let { issues.add(it) }
        if (issues.isNotEmpty()) {
            return Either.Left(CategoryValidationFailed(issues))
        }
        dryRunContext.completeIfDryRun()
        val entity = command.parentCategoryId?.let { repository.findByIdOrNull(it) }
                .let { categoryFactory.toEntity(command, it) }
        return Either.Right(repository.save(entity).toModel())
                .also { domainEventBus.post(CategoryCreatedEvent(it.value.categoryId)) }
    }

    @Transactional
    override fun updateCategoryDetails(
            categoryId: CategoryId,
            details: CategoryDetails
    ): Either<CategoryUpdateFailed, Category> {
        val category = repository.findByIdOrNull(categoryId)
                ?: return Either.Left(CategoryUpdateFailed.CategoryNotFound(categoryId))
        val issues = validationService.validate(details).toMutableList()
        slugUniquenessValidator.validate(details.slug, inContextOfCategoryId = categoryId)
                ?.let { issues.add(it) }
        if (issues.isNotEmpty()) {
            return Either.Left(CategoryUpdateFailed.CategoryValidationFailed(issues))
        }
        dryRunContext.completeIfDryRun()
        category.apply {
            name = details.name
            slug = details.slug
            description = details.description
        }
        return Either.Right(repository.save(category).toModel())
                .also { domainEventBus.post(CategoryDetailsUpdatedEvent(categoryId)) }
    }

    @Transactional
    override fun updateCategoryLocation(
            categoryId: CategoryId,
            location: CategoryLocation
    ): Either<CategoryUpdateFailed, Category> {
        val category = repository.findByIdOrNull(categoryId)
                ?: return Either.Left(CategoryUpdateFailed.CategoryNotFound(categoryId))
        val issues = validationService.validate(location).toMutableList()
        location.parentCategoryId?.let { parentCategoryValidator.validate(it, category.toModel()) }
                ?.let { issues.add(it) }
        positionValidator.validate(location, category)?.let { issues.add(it) }
        if (issues.isNotEmpty()) {
            return Either.Left(CategoryUpdateFailed.CategoryValidationFailed(issues))
        }
        dryRunContext.completeIfDryRun()

        val currentSiblings = siblings(category.parentCategory?.id)
                .filter { it.id != categoryId }
                .toMutableList()
        if (category.parentCategory?.id == location.parentCategoryId) {
            currentSiblings.add(location.position, category)
        }
        currentSiblings.forEachIndexed { index, sibling -> sibling.position = index }
        currentSiblings.forEach { repository.save(it) }

        if (category.parentCategory?.id != location.parentCategoryId) {
            val newSiblings = siblings(location.parentCategoryId).toMutableList()
            newSiblings.add(location.position, category)
            newSiblings.forEachIndexed { index, sibling -> sibling.position = index }
            newSiblings.forEach { repository.save(it) }
        }

        category.position = location.position
        val newParent = location.parentCategoryId?.let { repository.findByIdOrNull(it) }
        category.parentCategory = newParent
        return Either.Right(repository.save(category).toModel())
                .also { domainEventBus.post(CategoryLocationUpdatedEvent(it.value.categoryId)) }
    }

    private fun siblings(parentCategoryId: CategoryId?) =
            parentCategoryId?.let { repository.findByIdOrNull(it) }
                    ?.childrenCategories ?: repository.findTopCategories()

    @Transactional
    override fun deleteCategory(
            categoryId: CategoryId,
            cascade: Boolean
    ): Either<CategoryNotFound, Unit> {
        val category = repository.findByIdOrNull(categoryId) ?: return Either.Left(
                CategoryNotFound(categoryId)
        )
        dryRunContext.completeIfDryRun()

        val siblings = siblings(category.parentCategory?.id).toMutableList()
        if (!cascade) {
            siblings.addAll(category.position, category.childrenCategories)
        }
        siblings.filter { it.id != categoryId }
                .forEachIndexed { i, sibling ->
                    sibling.position = i
                    sibling.parentCategory = category.parentCategory
                    repository.save(sibling)
                }

        if (cascade) {
            category.childrenCategories.forEach {
                repository.delete(it)
                domainEventBus.post(CategoryRemovedEvent(it.id!!))
            }
        }
        repository.delete(category)
        domainEventBus.post(CategoryRemovedEvent(categoryId))
        return Either.Right(Unit)
    }


    override fun getCategoriesTree(): List<CategoryWithChildren> =
            repository.findTopCategories().map { it.toWithChildrenModel() }

    override fun getCategoriesDFS(): List<Category> =
            repository.findTopCategories().map { topCategory ->
                listOf(topCategory.toModel()) + topCategory.childrenCategories.map { it.toModel() }
            }.flatten()

    override fun getCategoryBySlug(slug: String): Category? =
            repository.findAllBySlug(slug)?.toModel()

    override fun getCategory(categoryId: CategoryId): Category? =
            repository.findByIdOrNull(categoryId)?.toModel()
}