/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.signin.api

import org.jbb.swanframework.application.MemberId
import org.jbb.swanframework.eventbus.DomainEvent

// Events
class SignInSuccessEvent(val memberId: MemberId, val createdSessionId: String) : DomainEvent() {
    override fun toString(): String {
        return "SignInSuccessEvent(memberId='$memberId', createdSessionId='$createdSessionId') ${super.toString()}"
    }
}

class SignInFailedEvent(val memberId: MemberId?, val login: String) : DomainEvent() {
    override fun toString(): String {
        return "SignInFailedEvent(memberId=$memberId, login='$login') ${super.toString()}"
    }
}