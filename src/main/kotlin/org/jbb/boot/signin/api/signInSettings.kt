/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.signin.api

import org.jbb.swanframework.eventbus.DomainEvent

// Operations
interface SignInSettingsFacade {

    fun getSignInSettings(): SignInSettings

    fun updateSignInSettings(signInSettings: SignInSettings): SignInSettings

}

// Models
data class SignInSettings(
        val loginType: LoginType,
        val caseSensitive: Boolean
)

enum class LoginType {
    USERNAME, EMAIL, USERNAME_OR_EMAIL
}

// Events
class SignInSettingsChangedEvent : DomainEvent() {
    override fun toString(): String {
        return "SignInSettingsChangedEvent() ${super.toString()}"
    }
}