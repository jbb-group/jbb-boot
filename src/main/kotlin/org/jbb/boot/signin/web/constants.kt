/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.signin.web

internal const val SIGN_IN_SETTINGS_API = "Sign in settings API"
const val AUTHENTICATION_API = "Authentication API"

internal const val SIGN_IN_SETTINGS = "/sign-in-settings"

