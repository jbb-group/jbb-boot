/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.signin.web.settings

import io.swagger.v3.oas.annotations.media.Schema
import org.jbb.boot.signin.api.LoginType
import javax.validation.constraints.NotNull

@Schema(name = "SignInSettings")
internal data class SignInSettingsDto(
        @field:NotNull val loginType: LoginType,
        @field:NotNull val loginCaseSensitive: Boolean
)