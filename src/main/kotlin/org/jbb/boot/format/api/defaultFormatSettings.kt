/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.boot.format.api

import arrow.core.Either
import org.jbb.swanframework.eventbus.DomainEvent
import org.jbb.swanframework.validation.ValidationFailed
import org.jbb.swanframework.validation.ValidationIssue
import javax.validation.constraints.NotBlank

// Operations
interface DefaultFormatSettingsFacade {

    fun getDefaultFormatSettings(): DefaultFormatSettings

    fun updateDefaultFormatSettings(defaultFormatSettings: DefaultFormatSettings):
            Either<UpdateDefaultFormatSettingsFailed, DefaultFormatSettings>
}

// Errors
class UpdateDefaultFormatSettingsFailed(override val issues: List<ValidationIssue>) :
        RuntimeException(),
        ValidationFailed

// Models
data class DefaultFormatSettings(
        @field:NotBlank val dateTimeFormat: String,
        @field:NotBlank val durationFormat: String
)

// Events
class DefaultFormatSettingsChangedEvent : DomainEvent() {
    override fun toString(): String {
        return "DefaultFormatSettingsChangedEvent() ${super.toString()}"
    }
}