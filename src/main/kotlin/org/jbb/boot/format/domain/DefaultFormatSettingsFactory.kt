/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.domain

import org.jbb.boot.format.api.DefaultFormatSettings

internal object DefaultFormatSettingsFactory {

    fun toModel(defaultFormats: List<FormatSettingEntity>): DefaultFormatSettings =
            DefaultFormatSettings(
                    dateTimeFormat = getFormatValue(defaultFormats, FormatType.DATE_TIME),
                    durationFormat = getFormatValue(defaultFormats, FormatType.DURATION)
            )

    private fun getFormatValue(
            defaultFormats: List<FormatSettingEntity>,
            formatType: FormatType
    ): String = defaultFormats.find { it.formatType == formatType }
            ?.let { it.formatValue }
            ?: throw IllegalStateException("$formatType not found in default formats")
}