/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.web

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.jbb.boot.format.api.MemberFormatSettingsFacade
import org.jbb.boot.format.api.UpdateMemberFormatSettingsFailed
import org.jbb.boot.format.web.FormatSettingsWebTranslator.toDto
import org.jbb.boot.format.web.FormatSettingsWebTranslator.toModel
import org.jbb.swanframework.arrow.assertRight
import org.jbb.swanframework.dryrun.DryRunnable
import org.jbb.swanframework.security.SecurityConstants.IS_AUTHENTICATED
import org.jbb.swanframework.web.HttpRequestContext
import org.jbb.swanframework.web.RestConstants.API_V1
import org.jbb.swanframework.web.RestConstants.ME
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name = FORMAT_SETTINGS_API)
@RequestMapping(API_V1 + ME + FORMAT_SETTINGS, produces = [MediaType.APPLICATION_JSON_VALUE])
internal class MeFormatSettingsResource(
        private val memberFormatSettingsFacade: MemberFormatSettingsFacade,
        private val httpRequestContext: HttpRequestContext
) {

    @Operation(
            summary = "Get my format settings",
            description = "Gets a format settings for a current member"
    )
    @PreAuthorize(IS_AUTHENTICATED)
    @GetMapping
    fun getMyFormatSettings(): MemberFormatSettingsDto {
        val currentMember = httpRequestContext.getCurrentMember()
        return toDto(memberFormatSettingsFacade.getFormatSettingsFor(currentMember!!.memberId))
    }

    @DryRunnable
    @PutMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize(IS_AUTHENTICATED)
    @Operation(
            summary = "Update my format settings",
            description = "Updates a format settings for a current member"
    )
    @Throws(UpdateMemberFormatSettingsFailed::class)
    fun updateMyFormatSettings(
            @RequestBody formatSettingsDto: MemberFormatSettingsDto): MemberFormatSettingsDto {
        val currentMember = httpRequestContext.getCurrentMember()
        val settings = toModel(formatSettingsDto)
        return toDto(
                memberFormatSettingsFacade
                        .updateFormatSettingsFor(currentMember!!.memberId, settings).assertRight()
        )
    }
}