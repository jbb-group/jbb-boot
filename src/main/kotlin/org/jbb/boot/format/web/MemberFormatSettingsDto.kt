/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot.format.web

import io.swagger.v3.oas.annotations.media.Schema

@Schema(name = "MemberFormatSettings")
internal data class MemberFormatSettingsDto(
        val dateTimeFormat: MemberFormatDto?,
        val durationFormat: MemberFormatDto?
)