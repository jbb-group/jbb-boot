/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.health

import org.springframework.stereotype.Service
import java.time.Instant

@Service
class HealthCheckService private constructor(private val healthCheckManager: HealthCheckManager) {

    fun latestHealthResult(): HealthResult = healthCheckManager.getHealthResult()

    fun forceUpdateHealth() = healthCheckManager.run {
        runHealthChecks()
        getHealthResult()
    }

}

data class HealthResult(val status: HealthStatus, val lastCheckedAt: Instant)

enum class HealthStatus {
    HEALTHY, UNHEALTHY;

    fun isHealthy() = this == HEALTHY

}