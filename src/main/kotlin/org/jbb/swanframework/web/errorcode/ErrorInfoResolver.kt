/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web.errorcode

import org.jbb.swanframework.web.ErrorInfo
import org.reflections.Reflections
import org.springframework.stereotype.Component

@Component
internal class ErrorInfoResolver {
    private val reflections = Reflections("org.jbb")

    fun getAllErrorInfosSorted(): List<ErrorInfo> =
            reflections.getSubTypesOf(ErrorInfo::class.java)
                    .filter { it.isEnum }
                    .flatMap { it.enumConstants.toList() }
                    .sortedBy { it.errorCode }

    fun findErrorInfoForException(exceptionClass: Class<*>): ErrorInfo? =
            getAllErrorInfosSorted()
                    .find { it.domainExceptionClasses.contains(exceptionClass) }


}