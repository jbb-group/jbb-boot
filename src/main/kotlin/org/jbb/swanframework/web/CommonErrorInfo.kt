/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web

import org.jbb.swanframework.web.errorcode.ErrorCodeResource
import org.springframework.http.HttpStatus

enum class CommonErrorInfo(
        override val status: HttpStatus, override val errorCode: String,
        override val message: String,
        override val domainExceptionClasses: Set<Class<out Exception>> = emptySet(),
) : ErrorInfo {
    // generic technical errors
    INTERNAL_ERROR(
            HttpStatus.INTERNAL_SERVER_ERROR,
            "IERR-001",
            "Internal error"
    ),
    NOT_READY(
            HttpStatus.SERVICE_UNAVAILABLE,
            "IERR-002",
            "Service is starting. Please try again in a moment"
    ),
    MISSING_PATH_VARIABLE(
            HttpStatus.INTERNAL_SERVER_ERROR, "IERR-003",
            "Missing path variable"
    ),
    CONVERSION_NOT_SUPPORTED(
            HttpStatus.INTERNAL_SERVER_ERROR, "IERR-004",
            "Conversion is not supported"
    ),
    MESSAGE_NOT_WRITABLE(
            HttpStatus.INTERNAL_SERVER_ERROR, "IERR-005",
            "Message cannot be written"
    ),
    ASYNC_REQUEST_TIMEOUT(
            HttpStatus.INTERNAL_SERVER_ERROR, "IERR-006",
            "Async request timeout"
    ),
    METHOD_NOT_SUPPORTED(
            HttpStatus.METHOD_NOT_ALLOWED, "GNRL-101",
            "Http method is not supported"
    ),
    UNSUPPORTED_MEDIA_TYPE(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "GNRL-102",
            "Given media type is not supported"),
    NOT_ACCEPTABLE_MEDIA_TYPE(HttpStatus.NOT_ACCEPTABLE, "GNRL-103",
            "Given media type is not acceptable"),
    MISSING_REQUEST_PARAMETER(HttpStatus.BAD_REQUEST, "GNRL-105",
            "Missing request parameter"),
    REQUEST_BINDING_ERROR(HttpStatus.BAD_REQUEST, "GNRL-106", "Request binding error"),
    TYPE_MISMATCH(HttpStatus.BAD_REQUEST, "GNRL-107", "Type mismatch"),
    MESSAGE_NOT_READABLE(HttpStatus.BAD_REQUEST, "GNRL-108", "Message is malformed"),
    VALIDATION_ERROR(HttpStatus.BAD_REQUEST, "GNRL-109", "Validation error"),
    MISSING_REQUEST_PART(HttpStatus.BAD_REQUEST, "GNRL-110", "Missing request part"),
    BIND_ERROR(HttpStatus.BAD_REQUEST, "GNRL-111", "Bind error"),
    NO_HANDLER_FOUND(HttpStatus.NOT_FOUND, "GNRL-112", "Not found"),
    UNRECOGNIZED_PROPERTY(HttpStatus.BAD_REQUEST, "GNRL-113", "Unrecognized property provided"),
    INVALID_FORMAT_PROPERTY(HttpStatus.BAD_REQUEST, "GNRL-114", "Invalid format of property"),
    JSON_PARSING_ERROR(HttpStatus.BAD_REQUEST, "GNRL-115", "Payload is not a valid json"),

    // authentication & authorization errors
    UNAUTHORIZED(HttpStatus.UNAUTHORIZED, "AUTH-001", "Access denied - unauthorized"),
    FORBIDDEN(HttpStatus.FORBIDDEN, "AUTH-002", "Access denied - forbidden"),
    BAD_CREDENTIALS(
            HttpStatus.UNAUTHORIZED, "AUTH-003",
            "Bad credentials have been provided"
    ),
    MEMBER_HAS_BEEN_LOCKED(
            HttpStatus.UNAUTHORIZED,
            "AUTH-004",
            "Member has been temporary locked due to many invalid sign in attempts"
    ),
    MEMBER_PASSWORD_EXPIRED(
            HttpStatus.FORBIDDEN,
            "AUTH-005",
            "Member's password has been expired. It must be changed immediately"
    ),

    // installation errors
    NOT_INSTALLED(HttpStatus.BAD_REQUEST, "INST-001", "Application not installed"),

    // error code errors
    ERROR_CODE_NOT_FOUND(
            HttpStatus.NOT_FOUND, "ERRC-001", "Error code not found",
            setOf(ErrorCodeResource.ErrorCodeNotFound::class.java)
    );

}