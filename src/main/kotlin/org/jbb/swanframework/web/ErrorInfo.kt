/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web

import org.springframework.http.HttpStatus

interface ErrorInfo {
    val status: HttpStatus
    val errorCode: String
    val message: String
    val domainExceptionClasses: Set<Class<out Exception>>
    val formattedMessage: String
        get() = "`$errorCode`: $message"
}