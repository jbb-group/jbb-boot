/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web.notreadable

import com.fasterxml.jackson.core.JsonParseException
import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.ErrorResponse
import org.jbb.swanframework.web.NotReadableExceptionHandler
import org.jbb.swanframework.web.createFrom
import org.springframework.stereotype.Component

@Component
internal class JsonParseHandler : NotReadableExceptionHandler<JsonParseException> {
    override fun getSupportedClass(): Class<JsonParseException> {
        return JsonParseException::class.java
    }

    override fun handle(ex: JsonParseException): ErrorResponse {
        return createFrom(CommonErrorInfo.JSON_PARSING_ERROR)
    }
}