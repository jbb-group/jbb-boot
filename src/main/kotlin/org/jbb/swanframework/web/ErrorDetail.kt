/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.web

import javax.validation.constraints.NotNull

data class ErrorDetail(
        val property: String?,
        val violationCode: String?,
        @field:NotNull
        val message: String,
        val context: Map<String, Any>? = emptyMap()
)