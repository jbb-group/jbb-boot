/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.startup

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.catalina.connector.Request
import org.apache.catalina.connector.Response
import org.apache.catalina.valves.ValveBase
import org.apache.tomcat.util.http.fileupload.IOUtils
import org.jbb.swanframework.web.CommonErrorInfo
import org.jbb.swanframework.web.createFrom
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.util.AntPathMatcher


internal class ProgressValve(private val objectMapper: ObjectMapper) : ValveBase() {

    init {
        StartUpListener.promise.thenRun { removeMyself() }
    }

    override fun invoke(request: Request, response: Response) {
        if (AntPathMatcher().match("/api/**", request.requestURI)) {
            objectMapper.writeValueAsBytes(createFrom(CommonErrorInfo.NOT_READY))
                    .let { response.outputStream.write(it) }
            response.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        } else {
            javaClass.getResourceAsStream("loading.html").use { loadingHtml ->
                IOUtils.copy(loadingHtml, response.outputStream)
            }
        }
        response.status = CommonErrorInfo.NOT_READY.status.value()
    }

    private fun removeMyself() {
        getContainer().pipeline.removeValve(this)
    }
}