/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.dryrun

import org.jbb.swanframework.web.HttpRequestContext
import org.springframework.stereotype.Component

@Component
class DryRunContext(private val httpRequestContext: HttpRequestContext) {

    fun completeIfDryRun() {
        val dryRunEnabled = httpRequestContext.getCurrentHttpRequest()
                ?.let { it.getParameter("dryRun") }
                ?.let { it.toBoolean() } ?: false

        if (dryRunEnabled) {
            throw DryRunEnabled()
        }
    }

}