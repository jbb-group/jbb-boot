/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.application.directory

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path
import javax.annotation.PostConstruct

internal class DefaultAppDirectory(private val appDirectoryProperties: AppDirectoryProperties)
    : AppDirectory {
    private val log: Logger = LoggerFactory.getLogger(this.javaClass)

    @PostConstruct
    override fun create(): Boolean {
        val appDirectoryCreated = createPathIfApplicable(path(), "Application directory")
        createPathIfApplicable(configPath(), "Application config directory")
        return appDirectoryCreated
    }

    private fun createPathIfApplicable(
            path: Path,
            name: String
    ): Boolean = if (Files.notExists(path)) {
        try {
            Files.createDirectory(path)
            log.info("{} {} created successfully", name, path)
            true
        } catch (e: IOException) {
            log.error("Could not create {} {}", name, path, e)
            throw IllegalStateException(e)
        }
    } else {
        log.debug("{} {} already exists. Skipping creating...", name, path)
        false
    }

    override fun path(): Path = appDirectoryProperties.appDirectoryPath()

    override fun configPath(): Path = appDirectoryProperties.appDirectoryPath().resolve(CONFIG)
}