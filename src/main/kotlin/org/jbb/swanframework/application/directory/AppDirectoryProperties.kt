/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.application.directory

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.io.File
import java.nio.file.Path

@ConstructorBinding
@ConfigurationProperties(prefix = "jbb.swan-framework.application.directory")
internal data class AppDirectoryProperties(
        val location: String = System.getProperty("user.home"),
        val name: String
) {
    fun appDirectoryPath(): Path = Path.of(File(location + File.separator + name).toURI())
}