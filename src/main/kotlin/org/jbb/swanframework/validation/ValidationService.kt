/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.validation

import org.springframework.stereotype.Component
import javax.validation.Validator

@Component
class ValidationService(private val validator: Validator) {

    fun validate(obj: Any): List<ValidationIssue> =
            validator.validate(obj).map {
                ValidationIssue(
                        name = it.propertyPath.toString(),
                        errorCode = null,
                        message = it.message
                )
            }
}