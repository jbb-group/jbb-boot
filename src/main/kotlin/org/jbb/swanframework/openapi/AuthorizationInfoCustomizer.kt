/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */

package org.jbb.swanframework.openapi

import io.swagger.v3.oas.models.Operation
import org.jbb.swanframework.security.SecurityConstants.IS_AN_ADMINISTRATOR
import org.jbb.swanframework.security.SecurityConstants.IS_AUTHENTICATED
import org.jbb.swanframework.security.SecurityConstants.PERMIT_ALL
import org.springdoc.core.customizers.OperationCustomizer
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component
import org.springframework.web.method.HandlerMethod

@Component
internal class AuthorizationInfoCustomizer : OperationCustomizer {

    override fun customize(operation: Operation, handlerMethod: HandlerMethod): Operation {
        if (!handlerMethod.hasMethodAnnotation(PreAuthorize::class.java)) {
            return operation
        }

        val authDescription =
                when (handlerMethod.getMethodAnnotation(PreAuthorize::class.java)?.value) {
                    PERMIT_ALL -> "<br/><br/>**Authorization:** Endpoint can be used by everyone. No authentication is required."
                    IS_AUTHENTICATED -> "<br/><br/>**Authorization:** Endpoint can be used only by members of the board."
                    IS_AN_ADMINISTRATOR -> "<br/><br/>**Authorization:** Endpoint can be used only by administrators of the board."
                    else -> ""
                }

        operation.description = operation.description + authDescription

        return operation
    }

}