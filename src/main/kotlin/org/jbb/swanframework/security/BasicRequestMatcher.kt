/*
 * Copyright (C) 2020 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.swanframework.security

import org.springframework.security.web.util.matcher.RequestMatcher
import javax.servlet.http.HttpServletRequest

internal class BasicRequestMatcher : RequestMatcher {
    override fun matches(request: HttpServletRequest): Boolean {
        val auth = request.getHeader("Authorization")
        return auth != null && auth.startsWith("Basic")
    }
}