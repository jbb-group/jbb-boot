/*
 * Copyright (C) 2021 the original author or authors.
 *
 * This file is part of jBB Application Project.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  You may obtain a copy of the License at
 *        http://www.apache.org/licenses/LICENSE-2.0
 */
package org.jbb.boot

import com.tngtech.archunit.core.importer.ImportOption.DoNotIncludeTests
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.junit.ArchTests

const val JBB_ROOT_PACKAGE = "org.jbb"

@AnalyzeClasses(packages = [JBB_ROOT_PACKAGE], importOptions = [DoNotIncludeTests::class])
class ArchitectureTest {

    @ArchTest
    val bootArchitectureTests = ArchTests.`in`(BootArchitectureRules::class.java)
}