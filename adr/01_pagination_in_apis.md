# Pagination in APIs

## Status

Accepted

## Context

Access to lists of data items must support pagination to protect the service against overload as well as for best client side iteration and batch processing experience. This holds true for all lists that are (potentially) larger than just a few hundred entries.

## Decision

* [Cursor/Limit-based](https://developer.twitter.com/en/docs/pagination) page iteration technique should be used in every endpoint with pagination
* Paging endpoint must accept `limit` and `cursor` request parameters
    * `limit` is an integer greater than zero. Maximum value may be dependent of returned resource
    * `cursor` is a string. Endpoint may validate incoming cursors
* Paging endpoints must return following structure:

```
{
   "parameters": {
      "limit": "...",     // limit provided by the client in the request
      "cursor": "...",    // cursor provided by the client in the request
      ...                 // all other request parameters provided by the client
   },
   "nextCursor": "...",   // a cursor value which client can use in the following request for getting next part of data. May be null if there's no more data.
   "content": [           // domain objects (up to "limit" value)
      ...
   ]
}
```

## Consequences

Cursor-based pagination is usually better and more efficient when compared to offset-based pagination. Consider the following trade-offs:

* If jumping to a particular page in a range (e.g., 51 of 100) is really a required use case, cursor-based navigation is not feasible
* Cursor-based navigation may not work if you need the total count of results