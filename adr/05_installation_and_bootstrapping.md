# Installation state and bootstrapping

## Status

Accepted

## Context

When application starts it requires on the early phrase of running a details about used database.
It's an example
of [bootstrapping](https://stackoverflow.com/questions/1254542/what-is-bootstrapping) operation.
Therefore we need to agree on the format and location of this special data.

## Decision

* Well configured application needs a **bootstrap file**
* **Bootstrap file** is placed under `config` folder
  in [application directory](04_application_directory.md) and it has a name `bootstrap.json`
* **Bootstrap file** contains details about connection to the database

_Example `config/bootstrap.json` for H2 file database:_

```json
{
   "database": {
      "type": "h2",
      "username": "dba",
      "password": "dba"
   }
}
```

_Example `config/bootstrap.json` for PostgreSQL database:_

```json
{
   "database": {
      "type": "postgres",
      "username": "dba",
      "password": "dba",
      "host": "localhost",
      "port": 5432,
      "databaseName": "default"
   }
}
```

* When bootstrap file is missing we say that application is not installed
* No business operation can be performed when application is not installed
* Installation can be performed via `POST /api/v1/installation` endpoint. Based on a provided
  details, application will create a proper `config/bootstrap.json` and restart itself

## Consequences

* Administrator is able to do an installation (creation of bootstrap file) via HTTP. No directly
  access to the server file storage is required
* Installation endpoint is available for everybody who knows an address to the running app. It might
  be some security risk
* In case of multi-instance environments it is important to provide the same `config/bootstrap.json`
  for every running instance of the app
* Password will be saved in a plain-text format, but in a next development cycles some encryption of
  credentials MUST be implemented