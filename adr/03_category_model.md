# Category model

## Status

Accepted

## Context

In a future we'd like to have a possibility to group multiple discussions. That is the main goal of
introducing categories. Category is meant to be just an attribute of the single discussion. We'd
like to keep both concepts (categories and discussions) decoupled as much as possible.

## Decision

* Category has a `name`, a `slug` and an optional `description`
* A [`slug`](https://stackoverflow.com/questions/19335215/what-is-a-slug) must be unique across
  whole board. It will be used in friendly URLs
* Categories can have a child categories. Naming:
    * Top category is a category which does not have a parent
    * Subcategory is a category which have a parent
* There is only one level of children permitted. So:
    * Top category can have multiple children (subcategories)
    * Subcategory cannot have any children
* Top categories and subcategories have its own position (`>= 0`) on a list.
* A subcategory can be moved to the different position, to the different parent or can be promoted
  to the "top level" and be a top category
* A top category can be moved to the different position or degraded to be a subcategory only if not
  have any children

## Consequences

More complex structure of categories is not possible.